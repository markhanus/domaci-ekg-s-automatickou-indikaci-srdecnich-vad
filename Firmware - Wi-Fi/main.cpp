
/*
SOČ 2024 - Domácí EKG s automatickou indikací srdečních vad
  Firmware ESP32 s komunikací po Wi-Fi

autor: Marek Hanus
licence: MIT License: https://mit-license.org/

*/

#include <Arduino.h>
#include <SPI.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include "ESPAsyncWebServer.h"  // Licence: LGPL-3.0 -> https://github.com/esphome/ESPAsyncWebServer.git 
#include "SPIFFS.h"
#include <ArduinoJson.h> // Licence: MIT -> https://github.com/bblanchon/ArduinoJson 
#include "esp_err.h" 
#include "esp_spiffs.h"
#include <driver/adc.h>

bool stop_preview = false;
const char* ssid = "EKG AP";
const char* password = "12345678"; 

AsyncWebServer server(80);
AsyncEventSource events("/events");

IPAddress local_ip(192, 168, 1, 63);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);


TaskHandle_t WebserverTask;


#define PREVIEW_TIME 2000
#define MEASURE_TIME 300000
#define AVG_NUM 21

//#define CONFIG_ASYNC_TCP_RUNNING_CORE 0



bool data_enable = true;
bool preview_mode = true;
int laststate = 1;

int16_t *adcPreview = (int16_t *) ps_malloc(PREVIEW_TIME * sizeof(int16_t));
int16_t *adcData = (int16_t *) ps_malloc(MEASURE_TIME * sizeof(int16_t));
int16_t *exportdata = (int16_t *) ps_malloc(12000 * sizeof(int16_t));


uint32_t datarate2[1];
float datarate;
float vbat_volty;

int adcRaw = 0;
int start, stop, cylce = 0;
int maxtime=0, mintime=10000000;
int start2 = 0, stop2=0;
bool previewmode = false, measuremode=false;
int milisekunda = 0;


int appendFile(fs::FS &fs, const char * path, const char * message);
void writeFile(fs::FS &fs, const char * path, const uint8_t * message, size_t size);
void deleteFile(fs::FS &fs, const char * path);


void run_preview()
{
  adcPreview = (int16_t *) ps_calloc(PREVIEW_TIME, sizeof(int16_t));
  adc1_config_channel_atten(ADC1_CHANNEL_5, ADC_ATTEN_0db);
  start = millis();
  for(int j = 0; j<PREVIEW_TIME; j++)
  {
    milisekunda = millis();
    for(int i = 0; i<21; i++) 
    {
      adcRaw += adc1_get_raw(ADC1_CHANNEL_5);
    }
    while (millis() == milisekunda);
    
    adcPreview[j] = adcRaw/21;
    adcRaw = 0;
  }
  stop = millis();
  datarate = (stop-start)/2;
}

void run_measure()
{
  previewmode = false;
  adcData = (int16_t *) ps_calloc(MEASURE_TIME, sizeof(int16_t));
  adc1_config_channel_atten(ADC1_CHANNEL_5, ADC_ATTEN_0db);
  start = millis();
  for(int j = 0; j<MEASURE_TIME; j++)
  {
    milisekunda = millis();
    for(int i = 0; i<21; i++) 
    {
      adcRaw += adc1_get_raw(ADC1_CHANNEL_5);
    }
    while (millis() == milisekunda);
    
    adcData[j] = adcRaw/21;
    adcRaw = 0;
  }

}
void handleRoot(AsyncWebServerRequest *request) 
{
  
  String html = R"(
    <html>

      <head>


        <script src="Chart.min.js"></script>
        <style>
           html {font-family: Arial; display: inline-block; text-align: center;}
          .topnav { overflow: hidden; background-color: #2f4468; color: white; font-size: 1.7rem; }
          .fcc-btn {
            background-color: #2f4468;
            color: white;
            padding: 15px 25px;
            font-size: 16px;

            text-decoration: none;
          }

          .fcc-btn:hover {
            background-color: #1f79c4;
          }
        </style>

        <script type='text/javascript'>
        
        if (!!window.EventSource) {
            var source = new EventSource('/events');
            
            source.addEventListener('open', function(e) {
            console.log("Events Connected");
            }, false);
            source.addEventListener('error', function(e) {
            if (e.target.readyState != EventSource.OPEN) {
              console.log("Events Disconnected");
            }
            }, false);
            
            source.addEventListener('message', function(e) {
            console.log("message", e.data);
            }, false);
            
            source.addEventListener('new_status', function(e) {
            
            var obj = JSON.parse(e.data);
            console.log("new_time", e.data);
            document.getElementById("datarate").innerHTML = obj.datarate.toFixed(0) + " Hz";
            //document.getElementById("power").innerHTML = obj.power.toFixed(0) + " mV";
            document.getElementById("cyclenum").innerHTML = obj.cyclenum + " s";

            }, false);
          }



          var chartData = {
            labels: [],
            datasets: [{
              label: 'Array Data Chart',
              borderColor: 'rgb(75, 192, 192)',
              data: [],
              fill: false,
            }]
          };

          var ctx = null;
          var chart = null;

          function initializeChart() {
            var canvas = document.getElementById('chartCanvas');
            ctx = canvas.getContext('2d');
            chart = new Chart(ctx, {
              type: 'line',
              data: chartData,
              options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                  x: [{
                    display: true,
                    scaleLabel: {
                      display: true,
                      labelString: 'Index'
                    }
                  }],
                  y: [{
                    display: true,
                    scaleLabel: {
                      display: true,
                      labelString: 'Value'
                    }
                  }]
                }
              }
            });
          }

          function updateChart() {
            fetch('/data')
              .then(response => response.json())
              .then(newData => {
                chartData.labels = Array.from({ length: newData.length }, (_, i) => i);
                chartData.datasets[0].data = newData;
                chart.update();
              });
          }
          
          function start_mereni()
          {
            var xhr = new XMLHttpRequest();
            xhr.open("GET", "/start", true);
            xhr.send();
          }

          

          
          document.addEventListener('DOMContentLoaded', function () {
            initializeChart();
            setInterval(updateChart, 2000); // Update chart every 2 seconds
          });
          
        </script>
        
      </head>
      <body>
        <div class="topnav">
        <h3>EKG MONITOR</h3>
        <h6><span>Datarate: </span> <span id="datarate"> Hz</span></h6>
        <h6><span>Time: </span> <span id="cyclenum"> s</span></h6>
        </div>
        
        <div style="padding: 25px 0px 25px 0px;"> <button class="fcc-btn" onclick='start_mereni()'>Spustit mereni</button> </div>
      
      <div style="padding: 25px 0px 25px 0px;">
        <canvas id='chartCanvas' width='800' height='400'></canvas>
      </div>
      <div> <a class="fcc-btn" href="/download">Stahnout vysledkovy soubor</a> </div> 
      </body>
    </html>
  )";

  request->send(200, "text/html", html);
}

void send_time()
{
  
  StaticJsonDocument<1000> root;
  String payload;
  root["cyclenum"] = millis()/1000;
  root["datarate"] = datarate;
  //root["power"] = vbat_volty;
  serializeJson(root, payload);
  Serial.print("event send :");
  serializeJson(root, Serial);
  events.send(payload.c_str(), "new_status", millis());
  
}

void handleData(AsyncWebServerRequest *request) {

  if(data_enable == true)
  {
    send_time();
    String jsonData = "[";

    for (int i = 0; i < PREVIEW_TIME; i++) {
      jsonData += "[" + String(i) + "," + String((adcPreview[i])) + "]";
      if (i < PREVIEW_TIME - 1) {
        jsonData += ",";
      }
    }

    jsonData += "]";
    request->send(200, "application/json", jsonData);
  }
}



void webserver_setup()
{
  server.on("/", HTTP_GET, handleRoot);
  server.on("/data", HTTP_GET, handleData);
  server.on("/Chart.min.js", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/Chart.min.js", "text/javascript");
  });

  server.on("/download", HTTP_GET, [](AsyncWebServerRequest *request){
    String path = "/vysledky.dat";

    File file = SPIFFS.open(path, FILE_READ);
    Serial.println("som tu");
    if (file) {
      stop_preview = true;
      request->send(SPIFFS, path, "application/json", true);
      stop_preview = false;
    }
    else {
      request->send(404, "text/plain", "File not found");
    }
  });

  server.on("/start", HTTP_GET, [](AsyncWebServerRequest *request){
    Serial.println("START button byl zmacknuty");
    preview_mode = false;
  });


  
  events.onConnect([](AsyncEventSourceClient *client){
    if(client->lastId()){
      Serial.printf("Client reconnected! Last message ID that it got is: %u\n", client->lastId());
    }
    // send event with message "hello!", id current millis
    // and set reconnect delay to 1 second
    client->send("hello!", NULL, millis(), 10000);
  });
  server.addHandler(&events);
  
  server.begin();
}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  


  WiFi.mode(WIFI_AP);
  // Set device as a Wi-Fi Station
  WiFi.softAP(ssid, password);
  WiFi.softAPConfig(local_ip, gateway, subnet);

  if(!SPIFFS.begin(true)){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }
  
  
  //Init
  if(psramInit()){
  Serial.println("\nPSRAM is correctly initialized");
  }else{
  Serial.println("PSRAM not available");
  }


  
  webserver_setup();    
  Serial.println(SPIFFS.totalBytes());
  Serial.println(SPIFFS.usedBytes());
}

void loop() {
  if(stop_preview == false)
  {
    run_preview();
    Serial.println(datarate);
    data_enable = true;

    delay(3000);
    
    
    data_enable = false;
    free(adcPreview);
  }
  
  if(preview_mode == false)
  {
    // Stop the server
    server.end();

    Serial.println("Mereni zacalo");
    run_measure();
    
    int c = 0;
    
    exportdata = (int16_t *) ps_calloc(12000, sizeof(int16_t));
    deleteFile(SPIFFS, "/vysledky.dat");
    
    bool append = false;
    for(int j=0; j<MEASURE_TIME;j++)
    {

      exportdata[c] = adcData[j];
      c++;
      if(c == 12000)
      {
        c=0;
        if(append == false)
        {
          size_t written;
          File file = SPIFFS.open("/vysledky.dat", FILE_WRITE);
          if(!file){
            Serial.println("Failed to open file for writing");
            return;
          }
          while((written = file.write((uint8_t *) exportdata, 12000*sizeof(int16_t))) == 0){
            Serial.println("Write failed");
          } 
          Serial.printf("File writed %d, should %d ;;; %i\n", written, 12000*sizeof(int16_t), j);
          file.close();
          append = true;
        } 
        else
        {
          size_t written;
          File file = SPIFFS.open("/vysledky.dat", FILE_APPEND);
          if(!file){
            Serial.println("Failed to open file for writing");
            return;
          }
          while((written = file.write((uint8_t *) exportdata, 12000*sizeof(uint32_t))) == 0){
            Serial.println("Append failed");
          } 
          Serial.printf("File appended %d, should %d ;;; %i\n", written, 12000*sizeof(uint32_t), j);
          file.close();
        }
      }
    }
    
    server.begin();

    preview_mode = true;

    ESP.restart();
  }
}


void writeFile(fs::FS &fs, const char * path, const uint8_t * message, size_t size)
{
  size_t written;
  Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if(!file){
    Serial.println("Failed to open file for writing");
    return;
  }
  if((written = file.write(message, size))){
    Serial.printf("File written %d, should %d\n", written, size);
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

int appendFile(fs::FS &fs, const char * path, const char * message){
  
  int state = 0;
  File file = fs.open(path, FILE_APPEND);
  if(!file){
    Serial.println("Failed to open file for appending");
    return 0;
  }
  if(file.print(message)){
      state = 1;
  } else {
    state = 0;
    Serial.println("write failed");
  }
  file.close();

  if(state) return 1;
  else return 0;
}

void deleteFile(fs::FS &fs, const char * path){
  Serial.printf("Deleting file: %s\n", path);
  if(fs.remove(path)){
    Serial.println("File deleted");
  } else {
    Serial.println("Delete failed");
  }
}
