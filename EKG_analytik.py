
"""
SOČ 2024 - Domácí EKG s automatickou indikací srdečních vad
  Vyhodnocovací program EKG ANALYTIK

autor: Marek Hanus
licence: MIT License (MIT): https://mit-license.org/

"""


import customtkinter as ctk # MIT License (MIT): https://github.com/TomSchimansky/CustomTkinter | Tom Schimansky
from CTkMessagebox import CTkMessagebox # CC0-1.0 licence: https://github.com/Akascape/CTkMessagebox 
from tkinter.filedialog import askopenfilename

import configparser # MIT License (MIT): https://pypi.org/project/configparser/ | Łukasz Langa

import numpy as np # Harris, C.R. et al., 2020. Array programming with NumPy. Nature, 585, pp.357–362.

import matplotlib.pyplot as plt # Python Software Foundation License (PSF) | John D. Hunter, Michael Droettboom
from matplotlib.patches import Ellipse
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)

from scipy import signal # BSD License (Copyright (c) 2001-2002 Enthought, Inc. 2003-2024, SciPy Developers. All rights reserved. Redistrib...)
from scipy.interpolate import interp1d

import openpyxl # MIT License (MIT)
from openpyxl.styles import Font
import struct

import os
import shutil
import sys

import threading
import serial # BSD License (BSD) | Chris Liechti
import re
import time

# Makowski, D., Pham, T., Lau, Z. J., Brammer, J. C., Lespinasse, F., Pham, H., Schölzel, C., & Chen, S. A. 
# (2021). NeuroKit2: A Python toolbox for neurophysiological signal processing. Behavior Research 
# Methods, 53(4), 1689-1696. https://doi.org/10.3758/s13428-020-01516-y
import neurokit2 as nk




ctk.set_appearance_mode("dark")  # Modes: "System" (standard), "Dark", "Light"
ctk.set_default_color_theme("blue")  # Themes: "blue" (standard), "green", "dark-blue"

class Draw_tkinter():
    def init(self):
        #init custotkinter window
        self.app = ctk.CTk()    
        self.app.title("Analýza EKG")
        
        self.width = 1920
        self.height = 1080
        
        self.app.geometry(str(1920)+"x"+str(self.height))
        
        #width = root.winfo_screenwidth()
        #height = root.winfo_screenheight()
        
        
        self.EKGcounter = 0
        self.EKGminicheck = False
        self.port = "/dev/ttyUSB0"
        self.monitor_speed = 115200
        self.EKGdatarate = 0
        self.monitor_speed_preset = 0
        
        self.setup_window()
        
    ###################################################################################################################################
    ###################################################################################################################################
    ######################################################## VYHODNOCENÍ EKG SOUBORŮ ##################################################
    ###################################################################################################################################
    ###################################################################################################################################
    def vynulovani_a_ulozeni(self):
        #přemazání hodnot po zmáčknutí tlačítka na vyhodnocení
        #všechny hodnoty v Entry boxech se uloží do proměnných, aby se v průběhu měření nemohly měnit TBD

        
        self.l_RRavg_bpm.configure(text="")
        self.l_RRmin_max.configure(text="")
        self.l_NADev.configure(text="")
        self.l_NADiff.configure(text="")
        self.l_sdnn_cv.configure(text="")
        self.l_rmssd.configure(text="")
        self.l_pnn50.configure(text="")
        self.l_MAD.configure(text="")
        self.l_TPR.configure(text="")
        self.l_vlf.configure(text="")
        self.l_lf.configure(text="")
        self.l_hf.configure(text="")
        self.l_f_total.configure(text="")
        self.l_lfhf.configure(text="")
        self.l_vlf_n.configure(text="")
        self.l_lf_n.configure(text="")
        self.l_hf_n.configure(text="")

        self.l_sd1.configure(text="")
        self.l_sd2.configure(text="")
        self.l_sd1sd2.configure(text="")
        self.l_pc_plocha.configure(text="")
        
        self.l_intervaly_arytmie.configure(text="")
        self.l_intervaly_bradykardie.configure(text="")
        self.l_intervaly_tachykardie.configure(text="")

        self.l_pocet_piku.configure(text="")
        self.l_piky_sin.configure(text="")
        self.l_piky_fibrilacni.configure(text="")
        self.l_piky_nadbytecne.configure(text="")
        self.l_piky_nevyhodnoceno.configure(text="")
        self.l_ok_piky.configure(text="")
        self.l_piky_velkeP.configure(text="")
        self.l_piky_malePR.configure(text="")
        self.l_piky_velkePR.configure(text="")
        self.l_piky_velkePxR.configure(text="")
        self.l_piky_velkeQ.configure(text="")
        self.l_piky_velkeQxR.configure(text="")
        self.l_piky_velkeQRS.configure(text="")
        self.l_piky_velkeS.configure(text="")
        self.l_piky_maleST.configure(text="")
        self.l_piky_velkeST.configure(text="")
        self.l_piky_velkeTxR.configure(text="")
        self.l_piky_velkeQTc.configure(text="")
        self.l_piky_maleQTc.configure(text="")
        self.statustextbox.configure(state="normal")
        self.statustextbox.delete("0.0", "end") 
        self.statustextbox.configure(state="disabled")

    def mereni(self):
        ###################################################################################################################################
        ########################################################### NAČTENÍ DAT ###########################################################
        ###################################################################################################################################
        self.vynulovani_a_ulozeni()
        
        hodnoty_x = []
        hodnoty_y = []
        i = 0
        datarate = 0
        
        peak_limit = int(self.e_peak_limit.get())*1000
        
        print("Dekóduji výsledkový soubor...")
        self.status_configure(text="Dekóduji výsledkový soubor...")
        
        
        print(self.frame_soubory_main.get())
        format_souboru = self.e_filename.get().split("/")
        format_souboru = format_souboru[len(format_souboru)-1]
        format_souboru = format_souboru.split(".")
        print(format_souboru[1])
        if self.frame_soubory_main.get() == "Vyhodnocení ze souboru" and format_souboru[1] != "csv":
            filename = self.e_filename.get()
            
            f = open(filename, mode="rb")

            # Reading file data with read() method
            data = f.read()
            
            while(1):  
                try:
                    U = struct.unpack("<i",data[(i*4):((i*4)+4)]) 
                    #print(int(U[0]))
                    hodnoty_y.append(U[0]*0.001164153218269)
                    i=i+1
                except:
                    break
                
            datarate = U[0] # poslední řádek výsledkového souboru obsahuje datarate, ulož ho do jiné proměnné a smaž ho z měření
            
            
            hodnoty_y.remove(datarate*0.001164153218269)
            
            time_konst = 1/1.2 # ADS1262 měří 1200 vzorků za sekundu -> jeden řádek = 1/1.2 ms
            
            self.vzorkovaci_frekvence = 1200
            
            print("Rychlost vzorkování [Hz]: "+str(datarate/100))
            #self.l_datarate.configure(text=str(round(datarate/100))+" Hz")
            
            print(filename)
        else:
            # Jestliže je soubor naměřen pomocí EKG mini (výsledek se nemusí dekódovat z uint8_t)
            
            if self.frame_soubory_main.get() == "Měření EKG":
                filename = self.EKGmini_filename.get()+'.csv' #### self.EKGmini_filename.get()+"/"+
                
            elif format_souboru[1] == "csv":
                filename = self.e_filename.get()
                
            else:
                self.status_configure(text="Soubor error, prosím restart...")
            
            print(filename)
            f = open(filename, mode="rb")
            data = f.read()
            
            # Dokóduj byte string na klasický řetězec
            text_string = data.decode('utf-8')
            
            # Najdi všechna čísla v souboru a ulož je do pole
            hodnoty_y = [int(match.group()) for match in re.finditer(r'\d+', text_string)]
            
            time_konst = 1
            self.vzorkovaci_frekvence = 1000
            """
            print("Rychlost vzorkování [Hz]: 1000")
            
            if(self.EKGdatarate != 0):
                self.l_datarate.configure(text=str(round((len(hodnoty_y)/int(self.EKGdatarate))*1000))+" Hz")
            else:
                self.l_datarate.configure(text="1000 Hz")
            """
            for i in range(len(hodnoty_y)):
                hodnoty_y[i] = (hodnoty_y[i]*950000)/3550

        print("dekodovani hotovo " + str(datarate))
        
        
        plt.style.use("default")
        
        for i in range(len(hodnoty_y)):
            hodnoty_x.append(i*time_konst) # vytvoř osu X s časovou značkou přepočítanou na milisekundy
        
        print("Počet řádků: "+str(len(hodnoty_y)))
        
        x = np.array(hodnoty_x) # ulož hodnoty měření do np.array proměnných
        y = np.array(hodnoty_y)
        

        vf_ms = self.vzorkovaci_frekvence/1000
        ###################################################################################################################################
        ######################################################## FILTRACE SIGNÁLU #########################################################
        ###################################################################################################################################
        if(self.ch_50hzfiltr.get() == 1):
            b, a = signal.iirnotch(int(self.e_frekvence50hz.get()), int(self.e_rad50hz.get()),fs=self.vzorkovaci_frekvence)
            y = signal.filtfilt(b, a, y)
            
        
        if(self.ch_butterworth.get() == 1):
            if(self.e_typ.get() == "bandpass" or self.e_typ.get() == "bandstop"):
                freq = self.e_frekvence.get().split(",")
                sos = signal.butter(int(self.e_rad.get()),[float(freq[0]), float(freq[1])], btype=self.e_typ.get(),fs=self.vzorkovaci_frekvence, output="sos")
                y = signal.sosfiltfilt(sos, y)
            else:
                sos = signal.butter(int(self.e_rad.get()),float(self.e_frekvence.get()), btype=self.e_typ.get(),fs=self.vzorkovaci_frekvence, output="sos")
                y = signal.sosfiltfilt(sos, y)
        
        
        ###################################################################################################################################
        ######################################################## NALEZENÍ R PÍKŮ ##########################################################
        ###################################################################################################################################
        peaks, _ = signal.find_peaks(y, height=peak_limit, distance=150) # detekuj píky měření
        
        if(len(peaks) == 0):
            CTkMessagebox(title="Warning", message='Nebyly nalezeny žádné pulzy, upravte kritérium "Mez identifikace pulzů"', icon="warning") 
            return
    

        peaks_in_ms = [j * time_konst for j in peaks] # přepočítej časové hodnoty píků na milisekundy
        dif = np.diff(peaks_in_ms) # urči časový rozdíl mezi píky
        
        


        print("Počet pulzů: " + str(len(peaks)))
        self.l_pocet_piku.configure(text=str(len(peaks)))
        self.statusbar_set(value=0.01)
        self.app.update()
        

        # vypočítání kritérií časové analýzy
        
        plt2_x = []
        plt2_y = []

        dif_avg = np.average(dif)
        min_dif = np.min(dif)
        max_dif = np.max(dif)
        bpm = 1000/dif_avg*60
        
        NADev = 0
        NADiff = 0
        pNN50=0
        RMSSD = 0
        SDNN = 0
        MAD = []
        
        for i in range(1, len(dif)):
            if abs(dif[i]-dif[i-1]) >= 50:
                pNN50=pNN50+1

            NADiff += abs(dif[i]-dif[i-1])/((len(dif)-1)*dif_avg)
            RMSSD += (np.power((dif[i]-dif[i-1]),2))

        
        pNN50 = (pNN50/len(dif))*100
        
        RMSSD = np.sqrt((1/(len(dif)-1))*RMSSD)
        nRMSSD = RMSSD/dif_avg
        
        
        
        for i in range(len(dif)):
            NADev += abs(dif[i]-dif_avg)/(len(dif)*dif_avg)
            SDNN += np.power((dif[i] - dif_avg),2)
            MAD.append(abs(dif[i]-np.median(dif)))

        SDNN = np.sqrt(1/((len(dif)-1)) * SDNN)
        CV = SDNN/dif_avg
        
        MAD = np.median(MAD)

        turning_point = 0
        for i in range(1,len(dif)-1):
            if(dif[i-1] > dif[i] and dif[i] < dif[i+1]):
                turning_point += 1
            elif(dif[i-1] < dif[i] and dif[i] > dif[i+1]):
                turning_point += 1
        
        TPR = turning_point/len(dif)

        # Zapiš vypočítané proměnné do aplikace
        self.l_RRavg_bpm.configure(text=str(round(dif_avg))+" ms / "+str(round(bpm))+" bpm")
        self.l_RRmin_max.configure(text=str(round(min_dif))+" / "+str(round(max_dif))+" ms")
        self.l_sdnn_cv.configure(text=str(round(SDNN))+ " ms / "+str(round(CV,3)))
        self.l_rmssd.configure(text=str(round(RMSSD))+" ms / "+str(round(nRMSSD,3)))
        self.l_pnn50.configure(text=str(round(pNN50))+" %")
        self.l_NADev.configure(text=str(round(NADev,3)))
        self.l_NADiff.configure(text=str(round(NADiff,3)))
        self.l_MAD.configure(text=str(round(MAD,2)))
        self.l_TPR.configure(text=str(round(TPR,2)))

        # vytvoř složku s výsledky
        if(self.frame_soubory_main.get() == "Vyhodnocení ze souboru" and format_souboru[1] != "csv"):
            slozka = self.e_slozka.get() #"EKG"+strftime("%y%m%d_%H%M%S")
            
            try:
                shutil.rmtree(slozka)
            except:
                print("slozka neexistuje")
            
            
            os.mkdir(slozka)
            
            shutil.copyfile(filename, slozka+"/"+slozka+".dat") # zkopíruj původní soubor s daty do složky s výsledky
        else:
            if self.frame_soubory_main.get() != "Vyhodnocení ze souboru":
                slozka = self.EKGmini_filename.get()
            
            else:
                slozka = self.e_slozka.get()

            try:
                shutil.rmtree(slozka)
            except:
                print("slozka neexistuje")
            
            
            os.mkdir(slozka)
            shutil.copyfile(filename, slozka+"/"+slozka+".csv") # zkopíruj původní soubor s daty do složky s výsledky
        
        


        ###################################################################################################################################
        ################################# VYTVOŘENÍ CELKOVÉHO GRAFU, POINCARÉ A FREKVENČNÍHO SPEKTRA ######################################
        ###################################################################################################################################
        if self.chbx_export[0].get() == 1:
            # vytvoř graf s hodnotami celého měření
            fig = plt.figure()
            plt.plot(x,y)
            plt.xlabel("Čas [ms]")
            plt.ylabel("Napětí [uV]")
            plt.plot(peaks_in_ms, y[peaks], "x")
            
            fig.set_size_inches(18.5, 10.5)
            if self.chbx_export[0].get() == 1:
                plt.savefig(slozka+'/'+slozka+'celkovy_graf.png', dpi=250)

            plt.close(fig)

        
        rr_n = dif[:-1]
        rr_n1 = dif[1:]
        
        sd1 = np.sqrt(0.5) * np.std(rr_n1 - rr_n)
        sd2 = np.sqrt(0.5) * np.std(rr_n1 + rr_n)
        
        pc_plocha = np.pi * sd1 * sd2


        self.l_sd1.configure(text=str(round(sd1))+" ms")
        self.l_sd2.configure(text=str(round(sd2))+" ms")
        self.l_sd1sd2.configure(text=str(round((sd1/sd2),2)))
        self.l_pc_plocha.configure(text=str(round((pc_plocha)))+" ms2")
        
        m = np.mean(dif)

        self.statusbar_set(value=0.015)
        self.status_configure(text="Vytvářím obrázky Poincarého graf...")
        self.app.update()
        
        if self.chbx_export[4].get() == 1:
            # vytvoření Poincaré grafu, který zobrazuje odchylky píků
            fig = plt.figure()
            ax = plt.gca()
            ax.set_xlim([0, 2000])
            ax.set_ylim([0, 2000])
            
            plt.title("Poincarého graf")
            plt.xlabel("RR[x]")
            plt.ylabel("RR[x+1]")
            
            plt.scatter(rr_n, rr_n1)
            
            e1 = Ellipse((m, m), 2*sd1, 2*sd2, angle=-45, linewidth=1.2, fill=False, color="k")


            plt.gca().add_patch(e1)
            
            plt.axline((0, 0), (2000, 2000), linewidth=1, color='black', linestyle="dashdot")
            plt.axline((2000, 0), (0, 2000), linewidth=1, color='black', linestyle="dashdot")
            
            
            plt.text(50, 1000, "SD1: "+str(round(sd1))+" ms", fontsize="large")
            plt.text(50, 1000-(20*2.1*1), "SD2: "+str(round(sd2))+" ms", fontsize="large")
            plt.text(50, 1000-(20*2.1*2), "SD1/SD2: "+str(round(sd1/sd2,2)), fontsize="large")
            plt.text(50, 1000-(20*2.1*3), "S: "+str(round(pc_plocha))+" ms2", fontsize="large")




            fig.set_size_inches(18.5, 10.5)
            if self.chbx_export[4].get() == 1:
                plt.savefig(slozka+'/'+slozka+'_poincare.png', dpi=250)
        
            plt.close(fig)


        self.statusbar_set(value=0.02)
        
        self.status_configure(text="Vytvářím obrázky graf frekvenčního spektra...")
        self.app.update()
        
        # vytvoření grafu FFT spektra
        rr_ecg = dif
        
        x2 = np.cumsum(dif)/1000
        f_ecg = interp1d(x2, dif, kind='cubic', fill_value= 'extrapolate')
        # sample rate for interpolation
        fs = 4
        steps = 1 / fs
        
        xx_ecg = np.arange(0, np.max(x2), steps)
        rr_interpolated_ecg = f_ecg(xx_ecg)
        
        if self.chbx_export[3].get() == 1:
            fig = plt.figure()
            
            plt.title('Intervaly RR')
            plt.plot(x2, rr_ecg, color='k', markerfacecolor='#A999D1',marker='o', markersize=3)
            plt.xlabel('Čas měření [s]')
            plt.ylabel('RR [ms]')
            fig.set_size_inches(18.5, 10.5)
            if self.chbx_export[3].get() == 1:
                plt.savefig(slozka+'/'+slozka+'_Intervaly_RR.png', dpi=250)
            
            plt.close(fig)
        
        
        fxx, pxx = signal.welch(x=rr_interpolated_ecg, fs=fs, nperseg=256)
        
        # fit a function for plotting bands
        powerspectrum_f = interp1d(fxx, pxx, kind='cubic', fill_value= 'extrapolate')
        
        # setup frequency bands for plotting
        x_VLF = np.linspace(0, 0.04, 100)
        x_LF = np.linspace(0.04, 0.15, 100)
        x_HF = np.linspace(0.15, 0.4, 100)
        
        cond_VLF = (fxx >= 0) & (fxx < 0.04)
        cond_LF = (fxx >= 0.04) & (fxx < 0.15)
        cond_HF = (fxx >= 0.15) & (fxx < 0.4)
        
        VLF = np.trapz(pxx[cond_VLF], fxx[cond_VLF])
        LF = np.trapz(pxx[cond_LF], fxx[cond_LF])
        HF = np.trapz(pxx[cond_HF], fxx[cond_HF])

        total_F = VLF+LF+HF
        
        
        
        vlf = np.trapz(pxx[cond_VLF], fxx[cond_VLF])
        lf = np.trapz(pxx[cond_LF], fxx[cond_LF])
        hf = np.trapz(pxx[cond_HF], fxx[cond_HF])
        
        VLF_nu = 100 * vlf / (vlf +lf + hf)
        LF_nu = 100 * lf / (vlf +lf + hf)
        HF_nu = 100 * hf / (vlf +lf + hf)
        
        self.l_vlf.configure(text=str(round(VLF))+" ms2")
        self.l_lf.configure(text=str(round(LF))+" ms2")
        self.l_hf.configure(text=str(round(HF))+" ms2")
        
        self.l_vlf_n.configure(text=str(round(VLF_nu))+" %")
        self.l_lf_n.configure(text=str(round(LF_nu))+" %")
        self.l_hf_n.configure(text=str(round(HF_nu))+" %")
        
        self.l_lfhf.configure(text=str(round((LF/HF),2)))
        self.l_f_total.configure(text=str(round(total_F)) + " ms2")
        self.app.update()
        
        if self.chbx_export[5].get() == 1:
            fig = plt.figure(figsize=(15,6))
            plt.title("Frekvenční spektrum (Welchův periodogram)")


            y_text = max([max(powerspectrum_f(x_VLF)), max(powerspectrum_f(x_LF)), max(powerspectrum_f(x_HF))])
            y_text_add = (y_text/100)
            nasobic = 5

            plt.text(0.01, y_text, "VLF: "+str(round(VLF))+" ms2", fontsize="large")
            plt.text(0.01, y_text-(y_text_add*nasobic*1), "LF: "+str(round(LF))+" ms2", fontsize="large")
            plt.text(0.01, y_text-(y_text_add*nasobic*2), "HF: "+str(round(HF))+" ms2", fontsize="large")
            plt.text(0.01, y_text-(y_text_add*nasobic*3), "Total: "+str(round(total_F))+" ms2", fontsize="large")
            
            
            plt.text(0.07, y_text, "VLF: "+str(round(VLF_nu))+" %", fontsize="large")
            plt.text(0.07, y_text-(y_text_add*nasobic*1), "LF: "+str(round(LF_nu))+" %", fontsize="large")
            plt.text(0.07, y_text-(y_text_add*nasobic*2), "HF: "+str(round(HF_nu))+" %", fontsize="large")
            plt.text(0.07, y_text-(y_text_add*nasobic*3), "LF/HF: "+str(round(LF/HF,2)), fontsize="large")
            


            plt.gca().fill_between(x_VLF, powerspectrum_f(x_VLF), alpha=0.5, color="#F5866F", label="VLF")
            plt.gca().fill_between(x_LF, powerspectrum_f(x_LF), alpha=0.5, color="#51A6D8", label="LF")
            plt.gca().fill_between(x_HF, powerspectrum_f(x_HF), alpha=0.5, color="#ABF31F", label="HF")

            plt.gca().set_xlim(0, 0.4)
            plt.gca().set_ylim(0)
            plt.xlabel("Frekvence [Hz]")
            plt.ylabel("PSD [ms2/Hz]")
            
            plt.legend()
            
            if self.chbx_export[5].get() == 1:
                fig.savefig(slozka+'/'+slozka+'_Welch_periodogram.png')
            
            plt.close(fig)
                
        
        ###################################################################################################################################
        ################################################### VYTVOŘENÍ 10s INTERVALŮ #######################################################
        ###################################################################################################################################

        # vytvoření grafů hodnot rozdělených po 10 sekundách
        zbyvajiciradky = len(y)
        
        desetsekund = self.vzorkovaci_frekvence*10
        pocetintervalu = round((len(y)/desetsekund),0)
        
        self.statusbar_set(value=0.03)
        self.status_configure(text="Vytvářím obrázky 10s grafů 0/"+str(int(pocetintervalu)))
        self.app.update()
        
        lastpik = 0
        newpik = 0
        
        #if self.chbx_export[1].get() == 1 or self.chbx_zobrazeni[1].get() == 1:
            

        interval_NADev = [0] * 30
        interval_NADiff = [0] * 30
        interval_pNN50= [0]*30
        interval_RMSSD = [0]*30
        interval_SDNN = [0]*30
        interval_nRMSSD = [0]*30

        interval_dif_avg = [] 
        interval_min_dif = []
        interval_max_dif = []
        interval_bpm = []

        interval_TPR = [0]*30
        interval_MAD = [0]*30
        interval_CV = [0]*30

        interval_hodnoceni = [""]*30

        intervaly_arytmie = 0
        intervaly_bradykardie = 0
        intervaly_tachykardie = 0

        for interval_cycle in range(int(pocetintervalu)):
            fig = plt.figure()
            fig.set_size_inches(18.5, 10.5)
            plt.title("Interval č."+str(interval_cycle+1))
            plt.xlabel("Čas [ms]")
            plt.ylabel("Napětí [uV]")
            
            if(zbyvajiciradky >= desetsekund):
                l = 0
                
                while(peaks[l] < desetsekund+interval_cycle*desetsekund):
                    if(l < len(peaks)-1):
                        l=l+1
                    else:
                        l=l+1
                        break
                
                newpik = l

                if self.chbx_export[1].get() == 1:
                    plt.plot(x[interval_cycle*desetsekund:desetsekund+interval_cycle*desetsekund], y[interval_cycle*desetsekund:desetsekund+interval_cycle*desetsekund])
                    plt.plot(peaks_in_ms[lastpik:newpik], y[peaks[lastpik:newpik]], "x")
                
                zbyvajiciradky=zbyvajiciradky-desetsekund

                
                if(lastpik == 0):
                    interval_dif = dif[0:newpik-1]
                else:
                    interval_dif = dif[lastpik-1:newpik-1]
            else:
                if self.chbx_export[1].get() == 1:
                    plt.plot(x[interval_cycle*desetsekund:zbyvajiciradky+interval_cycle*desetsekund], y[interval_cycle*desetsekund:zbyvajiciradky+interval_cycle*desetsekund])
                    plt.plot(peaks_in_ms[lastpik:], y[peaks[lastpik:]], "x")
                zbyvajiciradky=0
                interval_dif = dif[lastpik-1:]

            if self.chbx_export[1].get() == 1:
                plt.axhline(peak_limit, color="black", linestyle="dashdot")
                
            
            interval_dif_avg.append(np.average(interval_dif))
            interval_min_dif.append(np.min(interval_dif))
            interval_max_dif.append(np.max(interval_dif))
            interval_bpm.append(1000/interval_dif_avg[interval_cycle]*60)
            interval_MAD2 = []
            
            
            for i in range(1, len(interval_dif)):
                if abs(interval_dif[i]-interval_dif[i-1]) >= 50:
                    interval_pNN50[interval_cycle]=interval_pNN50[interval_cycle]+1

                interval_NADiff[interval_cycle] += abs(interval_dif[i]-interval_dif[i-1])/((len(interval_dif)-1)*interval_dif_avg[interval_cycle])
                interval_RMSSD[interval_cycle] += (np.power((interval_dif[i]-interval_dif[i-1]),2))

            interval_pNN50[interval_cycle] = (interval_pNN50[interval_cycle]/len(interval_dif))*100
            
            interval_RMSSD[interval_cycle] = np.sqrt((1/(len(interval_dif)-1))*interval_RMSSD[interval_cycle])
            interval_nRMSSD[interval_cycle] = interval_RMSSD[interval_cycle]/interval_dif_avg[interval_cycle]

            for i in range(len(interval_dif)):
                interval_NADev[interval_cycle] += abs(interval_dif[i]-interval_dif_avg[interval_cycle])/(len(interval_dif)*interval_dif_avg[interval_cycle])
                interval_SDNN[interval_cycle] += np.power((interval_dif[i] - interval_dif_avg[interval_cycle]),2)
                interval_MAD2.append(abs(interval_dif[i]-np.median(interval_dif)))

            interval_SDNN[interval_cycle] = np.sqrt(1/((len(interval_dif)-1)) * interval_SDNN[interval_cycle])
            interval_CV[interval_cycle] = interval_SDNN[interval_cycle]/interval_dif_avg[interval_cycle]
            
            interval_MAD[interval_cycle] = np.median(interval_MAD2)

            interval_turning_point = 0
            for i in range(1,len(interval_dif)-1):
                if(interval_dif[i-1] > interval_dif[i] and interval_dif[i] < interval_dif[i+1]):
                    interval_turning_point += 1
                elif(interval_dif[i-1] < interval_dif[i] and interval_dif[i] > interval_dif[i+1]):
                    interval_turning_point += 1
            
            interval_TPR[interval_cycle] = interval_turning_point/len(interval_dif)


            # Zkontroluj kritéria intervalů, nastavané v PARAMETRY VYHODNOCENÍ -> Intervaly
            if_intervaly_NADev = float(self.e_NADev.get()) < interval_NADev[interval_cycle]
            if_intervaly_NADiff = float(self.e_NADiff.get()) < interval_NADiff[interval_cycle]
            if_intervaly_CV = float(self.e_CV.get()) < interval_CV[interval_cycle]
            if_intervaly_RMSSD = float(self.e_nRMSSD.get()) < interval_nRMSSD[interval_cycle]
            if_intervaly_MAD = float(self.e_MAD.get()) < interval_MAD[interval_cycle]
            if_intervaly_TPR = float(self.e_TPR.get()) < interval_TPR[interval_cycle]
            
            if(if_intervaly_NADev or if_intervaly_NADiff or if_intervaly_CV or if_intervaly_RMSSD or if_intervaly_MAD or if_intervaly_TPR):
                intervaly_arytmie += 1
                print("FS interval: ", interval_cycle+1)
                interval_hodnoceni[interval_cycle] += "_arytmie"

            if(interval_dif_avg[interval_cycle] > 1200):
                intervaly_bradykardie += 1
                print("Bradykardie interval: ", interval_cycle+1)
                interval_hodnoceni[interval_cycle] += "_bradykardie"
            
            if(interval_dif_avg[interval_cycle] < 600):
                intervaly_tachykardie += 1
                print("Tachykardie interval: ", interval_cycle+1)
                interval_hodnoceni[interval_cycle] += "_tachykardie"

            if(interval_hodnoceni[interval_cycle] == ""):
                interval_hodnoceni[interval_cycle] = "_OK"
            

            self.l_intervaly_arytmie.configure(text=str(intervaly_arytmie)+" / "+str(round((intervaly_arytmie/0.3)))+" %")
            self.l_intervaly_bradykardie.configure(text=str(intervaly_bradykardie)+" / "+str(round((intervaly_bradykardie/0.3)))+" %")
            self.l_intervaly_tachykardie.configure(text=str(intervaly_tachykardie)+" / "+str(round((intervaly_tachykardie/0.3)))+" %")
            self.app.update()

            # Zapiš výsledky do grafu intervalu píků
            y_text_add = (max(y[len(y)-(zbyvajiciradky+desetsekund):len(y)-zbyvajiciradky]) - min(y[len(y)-(zbyvajiciradky+desetsekund):len(y)-zbyvajiciradky]))/100
            y_text = max(y[len(y)-(zbyvajiciradky+desetsekund):len(y)-zbyvajiciradky])
            nasobic = 2.12
            if self.chbx_export[1].get() == 1:
                plt.text(x[len(y)-(zbyvajiciradky+desetsekund)], y_text, "Hodnocení:"+interval_hodnoceni[interval_cycle].replace("_", " "), fontsize="large", weight="bold")
                plt.text(x[len(y)-(zbyvajiciradky+desetsekund)], y_text-y_text_add*2*nasobic, "RR_avg: "+str(round(interval_dif_avg[interval_cycle]))+" ms", fontsize="large")
                plt.text(x[len(y)-(zbyvajiciradky+desetsekund)], y_text-y_text_add*3*nasobic, "RR_min: " + str(round(interval_min_dif[interval_cycle]))+" ms", fontsize="large")
                plt.text(x[len(y)-(zbyvajiciradky+desetsekund)], y_text-y_text_add*4*nasobic, "RR_max: " + str(round(interval_max_dif[interval_cycle]))+" ms", fontsize="large")
                plt.text(x[len(y)-(zbyvajiciradky+desetsekund)], y_text-y_text_add*5*nasobic, "HR: "+str(round(interval_bpm[interval_cycle]))+" bpm", fontsize="large")
                plt.text(x[len(y)-(zbyvajiciradky+desetsekund)], y_text-y_text_add*6*nasobic, "RMSSD: "+str(round(interval_RMSSD[interval_cycle]))+" ms", fontsize="large")
                plt.text(x[len(y)-(zbyvajiciradky+desetsekund)], y_text-y_text_add*7*nasobic, "SDNN: " + str(round(interval_SDNN[interval_cycle]))+ " ms", fontsize="large")
                plt.text(x[len(y)-(zbyvajiciradky+desetsekund)], y_text-y_text_add*8*nasobic, "pNN50: " + str(round(interval_pNN50[interval_cycle]))+" %", fontsize="large")

                plt.text(x[len(y)-(zbyvajiciradky+desetsekund)], y_text-y_text_add*10*nasobic, "nRMSSD: " +str(round(interval_nRMSSD[interval_cycle],3)), fontsize="large")
                plt.text(x[len(y)-(zbyvajiciradky+desetsekund)], y_text-y_text_add*11*nasobic, "CV: " +str(round(interval_CV[interval_cycle],3)), fontsize="large")
                plt.text(x[len(y)-(zbyvajiciradky+desetsekund)], y_text-y_text_add*12*nasobic, "NADev: " + str(round(interval_NADev[interval_cycle],3)), fontsize="large")
                plt.text(x[len(y)-(zbyvajiciradky+desetsekund)], y_text-y_text_add*13*nasobic, "NADiff: " + str(round(interval_NADiff[interval_cycle],3)), fontsize="large")
                plt.text(x[len(y)-(zbyvajiciradky+desetsekund)], y_text-y_text_add*14*nasobic, "MAD: " + str(round(interval_MAD[interval_cycle],2)), fontsize="large")
                plt.text(x[len(y)-(zbyvajiciradky+desetsekund)], y_text-y_text_add*15*nasobic, "TPR: " + str(round(interval_TPR[interval_cycle],2)), fontsize="large")
                

            if self.chbx_export[1].get() == 1:
                plt.savefig(slozka+'/'+slozka+'_Interval'+str(interval_cycle+1).zfill(2)+interval_hodnoceni[interval_cycle]+'.png', dpi=250)
            
            plt.close(fig)

            self.status_configure(text="Vytvářím obrázky 10s grafů "+str(interval_cycle+1)+"/"+str(int(pocetintervalu)))
            bar = 0.03+((i+1)*0.2)/pocetintervalu
            self.statusbar_set(value=bar)
            self.app.update()
            lastpik = newpik
        
        
        

        ###################################################################################################################################
        ################################################### VYTVOŘENÍ ANALÝZY PÍKŮ ########################################################
        ###################################################################################################################################
        print(len(peaks)-1)
        print("Vytvářím obrázky pulzů...")
        self.statusbar_set(value=0.23)
        self.status_configure(text="Vytvářím obrázky pulzů - 0/"+str(len(peaks)))
        self.app.update()
        

        # vytvoření grafů jednotlivých píků, které obsahují body určující P, T a R píky
        # grafy se uloží jako obrázek do souborů, poté se fig zavře, aby se graf nezobrazil při plt.show()
        #if self.chbx_export[2].get() == 1 or self.chbx_zobrazeni[2].get() == 1:
        #_, rpeaks = nk.ecg_peaks(y, sampling_rate=self.vzorkovaci_frekvence)
        _, waves_dwt = nk.ecg_delineate(y, peaks, sampling_rate=self.vzorkovaci_frekvence, method="dwt")
        
        RP_skip = 0
        if(waves_dwt['ECG_P_Offsets'][0] > waves_dwt['ECG_T_Onsets'][0]):
            P_skip = -1
        elif((str(waves_dwt['ECG_P_Offsets'][0]) == "nan" or str(waves_dwt['ECG_T_Onsets'][0]) == "nan") and waves_dwt['ECG_P_Offsets'][1] > waves_dwt['ECG_T_Onsets'][1]):
            P_skip = -1
        else:
            P_skip = 0

        print((waves_dwt['ECG_P_Offsets'][0] == "nan" or waves_dwt['ECG_T_Onsets'][0] == "nan"))
        print()
        print(len(waves_dwt['ECG_P_Onsets']))
        print(len(waves_dwt['ECG_P_Offsets']))
        print(len(waves_dwt['ECG_P_Offsets']))

        print(P_skip)
        
        # inicializuj proměnné pro měření částí píků
        Pduration = [0]*len(peaks)
        PRinterval = [0]*len(peaks)
        Qduration = [0]*len(peaks)
        Rduration = [0]*len(peaks)
        Sduration = [0]*len(peaks)
        QRSduration = [0]*len(peaks)
        QTcduration = [0]*len(peaks)
        QTduration = [0]*len(peaks)
        

        amplitudyPR = ["nevyhodnoceno"]*len(peaks)
        amplitudyJR = ["nevyhodnoceno"]*len(peaks)
        amplitudyTR = ["nevyhodnoceno"]*len(peaks)
        amplitudyQR = ["nevyhodnoceno"]*len(peaks)

        peak_hodnoceni = [""]*len(peaks)

        peak_OK = 0
        peak_nevyhodnoceno = 0
        peak_sin = 0
        #peak_nesin = 0
        peak_fibrilace = 0
        peak_nadbytecne = 0
        peak_velkeP = 0
        peak_malePR = 0
        peak_velkePR = 0
        peak_velkeQRS = 0
        peak_velkeQ = 0
        peak_velkeQxR = 0
        peak_velkePxR = 0
        peak_maleST = 0
        peak_velkeST = 0
        peak_velkeTxR = 0
        peak_velkeQTc = 0
        peak_maleQTc = 0
        skip_textlines = 0
        peak_velkeS = 0
        
        
        





        
        self.sirkaOP = int(500*vf_ms) # šířka okna píku
        
        for i in range(len(peaks)): #len(peaks)-1
            hodnoceni_v_obrazku_arytmie = ""
            hodnoceni_v_obrazku_velkePR = ""
            hodnoceni_v_obrazku_nadbytecne = ""
            hodnoceni_v_obrazku_velkeQRS = ""
            hodnoceni_v_obrazku_velkeP = ""
            hodnoceni_v_obrazku_velkeS = ""
            hodnoceni_v_obrazku_velkePxR = ""
            hodnoceni_v_obrazku_malePR = ""
            
            hodnoceni_v_obrazku_velkeQ = ""
            hodnoceni_v_obrazku_velkeQxR = ""
            hodnoceni_v_obrazku_JxR = ""


            hodnoceni_v_obrazku_QTc = ""
            hodnoceni_v_obrazku_velkeTxR = ""

            fig = plt.figure()
            
            plt.title("Pulz č."+str(i+1))
            plt.xlabel("Čas [ms]")
            plt.ylabel("Napětí [uV]")
            
            if(peaks[i]-self.sirkaOP < 0):
                zacatek_grafu = 0
            else:
                zacatek_grafu = peaks[i]-self.sirkaOP


            if(peaks[i]-self.sirkaOP < 0):
                zacatek_hledani = 0
            else:
                zacatek_hledani = peaks[i]-int(250*vf_ms)

            plt.plot(x[zacatek_grafu:peaks[i]+self.sirkaOP], y[zacatek_grafu:peaks[i]+self.sirkaOP])

            plt.axvline(x[peaks[i]]-int(60), color="red", linestyle="dashdot")
            plt.axvline(x[peaks[i]]+int(60), color="red", linestyle="dashdot")
            

            l1 = 320*np.sqrt(dif_avg/1000) - 60
            l2 = 450*np.sqrt(dif_avg/1000) - 60

            plt.axvline(x[peaks[i]]+l1, color="blue", linestyle="dashdot")
            plt.axvline(x[peaks[i]]+l2, color="blue", linestyle="dashdot")
            
            plt.axvline(x[peaks[i]]-(180), color="orange", linestyle="dashdot")
            plt.axvline(x[peaks[i]]-(280), color="orange", linestyle="dashdot")

            if(peaks[i] > int(60*vf_ms)): # když měření začíná těsně před prvním píkem, nejde vyhodnotit
            
                find_P_peaks,_ = signal.find_peaks(y[zacatek_hledani:peaks[i]-int(60*vf_ms)], prominence=int(self.e_Pprominence.get()))
                find_T_peaks,_ = signal.find_peaks(y[peaks[i]+int(60*vf_ms):peaks[i]+int(350*vf_ms)], prominence=int(self.e_Tprominence.get()))
                #print(len(find_P_peaks))
                #print(len(find_T_peaks))
                
                #print(x[zacatek_grafu:peaks[i]][find_P_peaks])
                

                plt.plot(x[zacatek_hledani:peaks[i]][find_P_peaks], y[zacatek_hledani:peaks[i]+self.sirkaOP][find_P_peaks], "x")
                plt.plot(x[peaks[i]+int(60*vf_ms):peaks[i]+self.sirkaOP][find_T_peaks], y[peaks[i]+int(60*vf_ms):peaks[i]+self.sirkaOP][find_T_peaks], "x")
                
                #plt.vlines(x[zacatek_grafu:peaks[i]][find_P_peaks], min(y[zacatek_grafu:peaks[i]+self.sirkaOP]), max(y[zacatek_grafu:peaks[i]+self.sirkaOP]), color="red")
                #plt.vlines(x[peaks[i]:peaks[i]+self.sirkaOP][find_T_peaks], min(y[zacatek_grafu:peaks[i]+self.sirkaOP]), max(y[zacatek_grafu:peaks[i]+self.sirkaOP]), color="red")
                
                if(1 == 1): #try:
                    if_P_Onsets = str(waves_dwt['ECG_P_Onsets'][i+P_skip+RP_skip]) != "nan"
                    if_P_Peaks = str(waves_dwt['ECG_P_Onsets'][i+P_skip+RP_skip]) != "nan"
                    if_P_Offsets = str(waves_dwt['ECG_P_Peaks'][i+P_skip+RP_skip]) != "nan"

                    if_Q_Peaks = str(waves_dwt['ECG_Q_Peaks'][i+RP_skip]) != "nan"

                    if_R_Onsets = str(waves_dwt['ECG_R_Onsets'][i+RP_skip]) != "nan"
                    if_R_Offsets = str(waves_dwt['ECG_R_Offsets'][i]) != "nan"

                    if_S_Peaks = str(waves_dwt['ECG_S_Peaks'][i]) != "nan"

                    if_T_Onsets = str(waves_dwt['ECG_T_Onsets'][i]) != "nan"
                    if_T_Peaks = str(waves_dwt['ECG_T_Peaks'][i]) != "nan"
                    if_T_Offsets = str(waves_dwt['ECG_T_Offsets'][i]) != "nan"
                    
                    if len(find_P_peaks) == 1 and len(find_T_peaks) == 1:
                        
                        peak_hodnoceni[i] += "_SIN"
                        if if_P_Onsets:
                            plt.plot(x[waves_dwt['ECG_P_Onsets'][i+P_skip+RP_skip]], y[waves_dwt['ECG_P_Onsets'][i+P_skip+RP_skip]],"*", color="green", label="ECG_P_Onsets")
                        if if_P_Peaks:
                            plt.plot(x[waves_dwt['ECG_P_Peaks'][i+P_skip+RP_skip]], y[waves_dwt['ECG_P_Peaks'][i+P_skip+RP_skip]],"*", color="orange", label="ECG_P_Peaks")
                        if if_P_Offsets:
                            plt.plot(x[waves_dwt['ECG_P_Offsets'][i+P_skip+RP_skip]], y[waves_dwt['ECG_P_Offsets'][i+P_skip+RP_skip]],"*", color="red", label="ECG_P_Offsets")

                    if if_Q_Peaks:
                        plt.plot(x[waves_dwt['ECG_Q_Peaks'][i+RP_skip]], y[waves_dwt['ECG_Q_Peaks'][i+RP_skip]],"*", color="orange", label="ECG_Q_Peaks")
                    
                    if if_R_Onsets:
                        plt.plot(x[waves_dwt['ECG_R_Onsets'][i+RP_skip]], y[waves_dwt['ECG_R_Onsets'][i+RP_skip]],"*", color="green", label="ECG_R_Onsets")
                        ECG_Q_Offset = waves_dwt['ECG_Q_Peaks'][i+RP_skip]
                        while ECG_Q_Offset < peaks[i]:
                            ECG_Q_Offset = ECG_Q_Offset+1
                            if(round(y[ECG_Q_Offset]) >= round(y[waves_dwt['ECG_R_Onsets'][i+RP_skip]])):
                                break
                        plt.plot(x[ECG_Q_Offset], y[ECG_Q_Offset],"*", color="blue", label="ECG_Q_Offset")

                    
                    plt.plot(x[peaks[i]], y[peaks[i]],"*", color="orange", label="ECG_R_Peaks")
                    if if_R_Offsets and if_S_Peaks:
                        plt.plot(x[waves_dwt['ECG_R_Offsets'][i]], y[waves_dwt['ECG_R_Offsets'][i]],"*", color="red", label="ECG_R_Offsets")

                        ECG_S_Onset = waves_dwt['ECG_S_Peaks'][i]
                        while ECG_S_Onset > peaks[i]:
                            ECG_S_Onset = ECG_S_Onset-1
                            if(round(y[ECG_S_Onset]) >= round(y[waves_dwt['ECG_R_Offsets'][i]])):
                                break

                        plt.plot(x[ECG_S_Onset], y[ECG_S_Onset],"*", color="blue", label="ECG_S_Onset")

                    if if_S_Peaks:
                        plt.plot(x[waves_dwt['ECG_S_Peaks'][i]], y[waves_dwt['ECG_S_Peaks'][i]],"*", color="orange", label="ECG_S_Peaks")

                    if len(find_P_peaks) == 1 and len(find_T_peaks) == 1:
                        skip_textlines = 0
                        if if_T_Onsets:
                            plt.plot(x[waves_dwt['ECG_T_Onsets'][i]], y[waves_dwt['ECG_T_Onsets'][i]],"*", color="green", label="ECG_T_Onsets")
                        if if_T_Peaks:
                            plt.plot(x[waves_dwt['ECG_T_Peaks'][i]], y[waves_dwt['ECG_T_Peaks'][i]],"*", color="orange", label="ECG_T_Peaks")
                        if if_T_Offsets:
                            plt.plot(x[waves_dwt['ECG_T_Offsets'][i]], y[waves_dwt['ECG_T_Offsets'][i]],"*", color="red", label="ECG_T_Offsets")
                    
                        if if_P_Offsets and if_P_Onsets:
                            Pduration[i] = (round(x[waves_dwt['ECG_P_Offsets'][i+P_skip+RP_skip]] - x[waves_dwt['ECG_P_Onsets'][i+P_skip+RP_skip]]))
                        if if_R_Onsets and if_P_Onsets:
                            PRinterval[i] = (round(x[waves_dwt['ECG_R_Onsets'][i+RP_skip]] - x[waves_dwt['ECG_P_Onsets'][i+P_skip+RP_skip]]))
                        
                        if if_T_Offsets and if_R_Onsets:
                            QTduration[i] = (round(x[waves_dwt['ECG_T_Offsets'][i]] - x[waves_dwt['ECG_R_Onsets'][i+RP_skip]]))
                            QTcduration[i] = (round(QTduration[i]/np.sqrt(dif_avg/1000)))
                    
                    else:
                        Pduration[i] = "nevyhodnoceno"
                        PRinterval[i] = "nevyhodnoceno"
                        QTduration[i] = "nevyhodnoceno"
                        QTcduration[i] = "nevyhodnoceno"
                        skip_textlines = 1


                    if if_R_Offsets and if_R_Onsets:
                        QRSduration[i] = (round(x[waves_dwt['ECG_R_Offsets'][i]] - x[waves_dwt['ECG_R_Onsets'][i+RP_skip]]))
                        Qduration[i] = (round(x[ECG_Q_Offset] - x[waves_dwt['ECG_R_Onsets'][i+RP_skip]]))
                        Sduration[i] = (round(x[waves_dwt['ECG_R_Offsets'][i]] - x[ECG_S_Onset]))
                        Rduration[i] = (round(x[ECG_S_Onset] - x[ECG_Q_Offset]))

                    
                    
                    y_text_add = (y[peaks[i]] - min(y[zacatek_grafu:peaks[i]+self.sirkaOP]))/100
                    y_text = y[peaks[i]]

                    if not (if_P_Onsets and if_P_Peaks and if_P_Offsets and if_Q_Peaks and if_R_Onsets and if_R_Offsets and if_S_Peaks and if_T_Offsets and if_T_Onsets and if_T_Peaks):
                        print("ERROR finding peaks with neurokit")
                        plt.text(x[zacatek_grafu], y_text-y_text_add*1*nasobic*1*nasobic, "Chyba při vyhodnocování pulzu", fontsize="large")
                        peak_nevyhodnoceno += 1
                        peak_hodnoceni[i] = "_nevyhodnoceno"
                    
                    else:

                        if len(find_P_peaks) == 1 and len(find_T_peaks) == 1:
                            peak_sin += 1
                    
                        else:
                            if len(find_P_peaks) == 0 or len(find_T_peaks) == 0:
                                peak_fibrilace += 1
                                peak_hodnoceni[i] += "_ATYP"
                                if intervaly_arytmie == 0:
                                    hodnoceni_v_obrazku_arytmie = " - AVRT/AVNRT/junkční tachykardie/flutter"
                                else:
                                    hodnoceni_v_obrazku_arytmie = " - Fibrilace síní"
                            else:
                                peak_nadbytecne += 1
                                peak_hodnoceni[i] += "_ATYP"
                                hodnoceni_v_obrazku_nadbytecne = " - Síňové extrasystoly"

                        nasobic = 2.1


                        if len(find_P_peaks) == 1 and len(find_T_peaks) == 1:
                            amplituda_piku_R = y[peaks[i]] - y[waves_dwt['ECG_P_Offsets'][i+P_skip+RP_skip]]
                            amplituda_piku_P = y[waves_dwt['ECG_P_Peaks'][i+P_skip+RP_skip]] - y[waves_dwt['ECG_P_Offsets'][i+P_skip+RP_skip]]
                            amplituda_piku_J = y[waves_dwt['ECG_R_Offsets'][i]] - y[waves_dwt['ECG_P_Offsets'][i+P_skip+RP_skip]]
                            amplituda_piku_T = y[waves_dwt['ECG_T_Peaks'][i]] - y[waves_dwt['ECG_P_Offsets'][i+P_skip]]
                            amplituda_piku_Q = y[waves_dwt['ECG_Q_Peaks'][i+RP_skip]] - y[waves_dwt['ECG_P_Offsets'][i+P_skip+RP_skip]]
                            
                            amplitudyPR[i] = round(amplituda_piku_P/amplituda_piku_R,2)
                            amplitudyJR[i] = round(amplituda_piku_J/amplituda_piku_R,2)
                            amplitudyTR[i] = round(amplituda_piku_T/amplituda_piku_R,2)
                            amplitudyQR[i] = round(amplituda_piku_Q/amplituda_piku_R,2)

                            if(Pduration[i] >= 120):
                                peak_hodnoceni[i] +=  "_>P"
                                hodnoceni_v_obrazku_velkeP = " - P-mitrale"
                                peak_velkeP += 1 
                            
                            if(PRinterval[i] < 120):
                                peak_hodnoceni[i] += "_<PR"
                                hodnoceni_v_obrazku_malePR = " - Preexcitace"
                                peak_malePR += 1

                            if(PRinterval[i] > 220):
                                peak_hodnoceni[i] += "_>PR"
                                if(intervaly_arytmie == 0):
                                    hodnoceni_v_obrazku_velkePR = " - AV blokáda I.stupně"
                                else:
                                    hodnoceni_v_obrazku_velkePR = " - AV blokáda II.stupně"
                                peak_velkePR += 1

                            if(amplitudyQR[i] <= -0.25):
                                peak_hodnoceni[i] += "_>Q/R"
                                hodnoceni_v_obrazku_velkeQxR = " - Ischemická choroba srdeční"
                                peak_velkeQxR += 1

                            
                            if(amplitudyPR[i] >= float(self.e_PR_mez.get())):
                                peak_hodnoceni[i] += "_>P/R"
                                hodnoceni_v_obrazku_velkePxR = " - P-pulmonale"
                                peak_velkePxR += 1

                            if(amplitudyJR[i] > float(self.e_JR_mez.get())):
                                peak_hodnoceni[i] += "_>ST"
                                peak_velkeST += 1
                                hodnoceni_v_obrazku_velkeJxR = " - Ischemická choroba srdeční"

                            if(amplitudyJR[i] <= (0-float(self.e_JR_mez.get()))):
                                peak_hodnoceni[i] += "_<ST"
                                peak_maleST += 1
                                hodnoceni_v_obrazku_velkeJxR = " - Ischemická choroba srdeční"

                            
                            if(amplitudyTR[i] >= float(self.e_TR_mez.get())):
                                peak_hodnoceni[i] += "_>T/R"
                                hodnoceni_v_obrazku_velkeTxR = "- Hyperkalémie nebo ischemická choroba srdeční"
                                peak_velkeTxR += 1

                            if(QTcduration[i] > 450):
                                peak_hodnoceni[i] += "_>QTc"
                                hodnoceni_v_obrazku_QTc = " - Riziko vzniku nebezpečných komorových arytmií"
                                peak_velkeQTc += 1

                            if(QTcduration[i] <= 320):
                                peak_hodnoceni[i] += "_<QTc"
                                hodnoceni_v_obrazku_QTc = " - Riziko vzniku nebezpečných komorových arytmií"
                                peak_maleQTc += 1
                        
                        if(QRSduration[i] >= 120):
                            peak_hodnoceni[i] += "_>QRS"
                            if intervaly_bradykardie == 0 and intervaly_tachykardie == 0:
                                hodnoceni_v_obrazku_velkeQRS = " - Blokáda Tawarova raménka"
                            if intervaly_bradykardie > 0:
                                hodnoceni_v_obrazku_velkeQRS = " - AV blokáda"
                            if intervaly_tachykardie > 0:
                                hodnoceni_v_obrazku_velkeQRS += " - AVRT nebo Komorová tachykardie"
                            
                            peak_velkeQRS += 1
                        
                        if(Qduration[i] >= 40):
                            peak_hodnoceni[i] += "_>Q"
                            peak_velkeQ += 1
                            hodnoceni_v_obrazku_velkeQ = "- Ischemická choroba srdeční"
                        
                        if(Sduration[i] >= 40):
                            peak_hodnoceni[i] += "_>S"
                            peak_velkeS += 1
                            hodnoceni_v_obrazku_velkeS = " - Blokáda pravého Tawarova raménka"
                        

                        if(peak_hodnoceni[i] == "_SIN"):
                            peak_hodnoceni[i] += "_OK"

                            peak_OK += 1

                        plt.text(x[zacatek_grafu], y_text-y_text_add*2*nasobic, "Doby pulzů: ", fontsize="large")
                        
                        plt.text(x[zacatek_grafu], y_text-y_text_add*(4-skip_textlines)*nasobic, "   Q = "+ str(Qduration[i])+ " ms"+hodnoceni_v_obrazku_velkeQ, fontsize="large")
                        plt.text(x[zacatek_grafu], y_text-y_text_add*(5-skip_textlines)*nasobic, "   R = "+ str(Rduration[i])+ " ms", fontsize="large")
                        plt.text(x[zacatek_grafu], y_text-y_text_add*(6-skip_textlines)*nasobic, "   S = "+ str(Sduration[i])+ " ms"+hodnoceni_v_obrazku_velkeS, fontsize="large")
                        plt.text(x[zacatek_grafu], y_text-y_text_add*(7-skip_textlines)*nasobic, "   QRS = "+ str(QRSduration[i])+ " ms"+hodnoceni_v_obrazku_velkeQRS, fontsize="large")


                        if len(find_P_peaks) == 1 and len(find_T_peaks) == 1:
                            plt.text(x[zacatek_grafu], y_text-y_text_add*3*nasobic, "   P = "  + str(Pduration[i])+ " ms"+hodnoceni_v_obrazku_velkeP, fontsize="large")

                            plt.text(x[zacatek_grafu], y_text-y_text_add*9*nasobic,  "Intervaly: ", fontsize="large")
                            plt.text(x[zacatek_grafu], y_text-y_text_add*10*nasobic,  "   PR = " + str(PRinterval[i])+ " ms"+hodnoceni_v_obrazku_velkePR+hodnoceni_v_obrazku_malePR, fontsize="large")
                            plt.text(x[zacatek_grafu], y_text-y_text_add*11*nasobic, "   QT = " + str(QTduration[i])+ " ms", fontsize="large")
                            plt.text(x[zacatek_grafu], y_text-y_text_add*12*nasobic, "   QTc = " + str(QTcduration[i])+ " ms"+hodnoceni_v_obrazku_QTc, fontsize="large")

                            

                            

                            plt.text(x[zacatek_grafu], y_text-y_text_add*14*nasobic,  "Poměry amplitud: ", fontsize="large")
                            plt.text(x[zacatek_grafu], y_text-y_text_add*15*nasobic,  "   P/R = " + str(amplitudyPR[i])+hodnoceni_v_obrazku_velkePxR, fontsize="large")
                            plt.text(x[zacatek_grafu], y_text-y_text_add*16*nasobic,  "   J/R = " + str(amplitudyJR[i])+hodnoceni_v_obrazku_JxR, fontsize="large")
                            plt.text(x[zacatek_grafu], y_text-y_text_add*17*nasobic,  "   T/R = " + str(amplitudyTR[i])+hodnoceni_v_obrazku_velkeTxR, fontsize="large")
                            plt.text(x[zacatek_grafu], y_text-y_text_add*18*nasobic,  "   Q/R = " + str(amplitudyQR[i])+hodnoceni_v_obrazku_velkeQxR, fontsize="large")

                        plt.text(x[zacatek_grafu], y_text-y_text_add*(20-skip_textlines*11)*nasobic,  "Počet pulzů:"+hodnoceni_v_obrazku_arytmie+hodnoceni_v_obrazku_nadbytecne, fontsize="large")
                        plt.text(x[zacatek_grafu], y_text-y_text_add*(21-skip_textlines*11)*nasobic,  "   před QRS: " + str(len(find_P_peaks)), fontsize="large")
                        plt.text(x[zacatek_grafu], y_text-y_text_add*(22-skip_textlines*11)*nasobic,  "     za QRS: " + str(len(find_T_peaks)), fontsize="large")                    
                #except:
                #    peak_nevyhodnoceno+=1
                #    peak_hodnoceni[i] = "_nevyhodnoceno"
  
            else:
                RP_skip = 0
                peak_nevyhodnoceno+=1
                peak_hodnoceni[i] = "_nevyhodnoceno"

            plt.text(x[zacatek_grafu], y_text, "Hodnocení: "+peak_hodnoceni[i].replace("_", " "), fontsize="large", weight="bold")
            fig.set_size_inches(18.5, 10.5)
            
            
            if self.chbx_export[2].get() == 1:
                plt.savefig(slozka+'/'+slozka+'_pulse'+str(i+1).zfill(3)+peak_hodnoceni[i].replace("/", "-").replace(">", "").replace("<", "")+'.png')
                
            
                
            plt.close(fig)
                
            self.status_configure(text="Vytvářím obrázky pulzů - "+str(i+1)+"/"+str(len(peaks)))
            bar = 0.23+((i+1)*0.57)/len(peaks)
            self.statusbar_set(value=bar)

            self.l_piky_sin.configure(text=str(peak_sin)+" / "+str(round((peak_sin/len(peaks))*100))+" %")
            self.l_piky_fibrilacni.configure(text=str(peak_fibrilace)+" / "+str(round((peak_fibrilace/len(peaks))*100))+" %")
            self.l_piky_nadbytecne.configure(text=str(peak_nadbytecne)+" / "+str(round((peak_nadbytecne/len(peaks))*100))+" %")
            self.l_ok_piky.configure(text=str(peak_OK)+" / "+str(round((peak_OK/len(peaks))*100))+" %")
            self.l_piky_nevyhodnoceno.configure(text=str(peak_nevyhodnoceno)+" / "+str(round((peak_nevyhodnoceno/len(peaks))*100))+" %")
            self.l_piky_velkeP.configure(text=str(peak_velkeP)+" / "+str(round((peak_velkeP/len(peaks))*100))+" %")

            self.l_piky_velkePR.configure(text=str(peak_velkePR)+" / "+str(round((peak_velkePR/len(peaks))*100))+" %")
            self.l_piky_malePR.configure(text=str(peak_malePR)+" / "+str(round((peak_malePR/len(peaks))*100))+" %")
            self.l_piky_velkeQRS.configure(text=str(peak_velkeQRS)+" / "+str(round((peak_velkeQRS/len(peaks))*100))+" %")
            self.l_piky_velkeQ.configure(text=str(peak_velkeQ)+" / "+str(round((peak_velkeQ/len(peaks))*100))+" %")
            self.l_piky_velkeS.configure(text=str(peak_velkeS)+" / "+str(round((peak_velkeS/len(peaks))*100))+" %")
            self.l_piky_velkeQxR.configure(text=str(peak_velkeQxR)+" / "+str(round((peak_velkeQxR/len(peaks))*100))+" %")
            self.l_piky_velkePxR.configure(text=str(peak_velkePxR)+" / "+str(round((peak_velkePxR/len(peaks))*100))+" %")
            self.l_piky_velkeST.configure(text=str(peak_velkeST)+" / "+str(round((peak_velkeST/len(peaks))*100))+" %")
            self.l_piky_velkeTxR.configure(text=str(peak_velkeTxR)+" / "+str(round((peak_velkeTxR/len(peaks))*100))+" %")
            self.l_piky_maleST.configure(text=str(peak_maleST)+" / "+str(round((peak_maleST/len(peaks))*100))+" %")
            self.l_piky_velkeQTc.configure(text=str(peak_velkeQTc)+" / "+str(round((peak_velkeQTc/len(peaks))*100))+" %")
            self.l_piky_maleQTc.configure(text=str(peak_maleQTc)+" / "+str(round((peak_maleQTc/len(peaks))*100))+" %")
            
            self.app.update()


        # Celkové vyhodnocení kritérií.
        
        textmessage = []
        procentamessage = []

        pocet_piku_procenta = len(peaks)/100

        if(intervaly_bradykardie > 0):
            textmessage.append("Bradykardie ("+str(round(intervaly_bradykardie/0.3))+"% výskyt) \n")
            procentamessage.append(round(intervaly_bradykardie/0.3))
        if(intervaly_arytmie > 0 and intervaly_bradykardie > 0):
            textmessage.append("AV nebo SA blokády ("+str(min([round(intervaly_bradykardie/0.3), round(intervaly_arytmie/0.3)]))+"% výskyt)\n")
            procentamessage.append(min([round(intervaly_bradykardie/0.3), round(intervaly_arytmie/0.3)]))
        if intervaly_tachykardie > 0:
            textmessage.append("Tachykardie ("+str(round(intervaly_tachykardie/0.3))+"% výskyt) \n")
            procentamessage.append(round(intervaly_tachykardie/0.3))
        
        if intervaly_arytmie > 0 and peak_fibrilace > 0:
            textmessage.append("Fibrilace síní ("+str(min([round(intervaly_arytmie/0.3), round(peak_fibrilace)]))+"% výskyt) \n")
            procentamessage.append(min([round(intervaly_arytmie/0.3), round(peak_fibrilace)]))
        
        if intervaly_arytmie == 0 and peak_fibrilace > 0:
            textmessage.append("AVRT / AVNRT / junkční tachykardie / flutter síní ("+str(round(peak_fibrilace/pocet_piku_procenta))+"% výskyt) \n")
            procentamessage.append(round(peak_fibrilace/pocet_piku_procenta))

        if intervaly_arytmie == 0 and peak_velkePR > 0:
            textmessage.append("AV blokáda I.stupně ("+str(round(peak_velkePR/pocet_piku_procenta))+"% výskyt) \n") # Nemělo by to být čistě velkePR %
            procentamessage.append(min([round(intervaly_arytmie/0.3), round(peak_velkePR/pocet_piku_procenta)]))

        if intervaly_arytmie > 0 and peak_velkePR > 0:
            textmessage.append("AV blokáda II.stupně ("+str(min([round(intervaly_arytmie/0.3), round(peak_velkePR/pocet_piku_procenta)]))+"% výskyt) \n") # Nemělo by to být čistě velkePR %
            procentamessage.append(min([round(intervaly_arytmie/0.3), round(peak_velkePR/pocet_piku_procenta)]))

        if peak_nadbytecne > 0:
            textmessage.append("Síňové extrasystoly ("+str(round(peak_nadbytecne/pocet_piku_procenta))+"% výskyt) \n")
            procentamessage.append(round(peak_nadbytecne/pocet_piku_procenta))

        if peak_velkeQRS > 0 and intervaly_bradykardie > 0:
            textmessage.append("AV blokáda ("+str(min([round(peak_velkeQRS/pocet_piku_procenta), round(intervaly_bradykardie/0.3)]))+"% výskyt)\n")
            procentamessage.append(min([peak_velkeQRS/pocet_piku_procenta, intervaly_bradykardie/0.3]))

        if peak_velkeQRS > 0 and intervaly_tachykardie == 0 and intervaly_arytmie == 0 and intervaly_bradykardie == 0:
            textmessage.append("Blokáda Tawarova raménka ("+str(min([round(peak_velkeQRS/pocet_piku_procenta), round(intervaly_tachykardie/0.3), round(intervaly_bradykardie/0.3), round(intervaly_arytmie/0.3)]))+"% výskyt)\n")
            procentamessage.append(min([round(peak_velkeQRS/pocet_piku_procenta), round(intervaly_tachykardie/0.3), round(intervaly_bradykardie/0.3), round(intervaly_arytmie/0.3)]))

        if peak_velkeQRS > 0 and intervaly_tachykardie > 0:
            textmessage.append("AVRT nebo Komorová tachykardie  ("+str(min([round(intervaly_tachykardie/0.3), round(peak_velkeQRS/pocet_piku_procenta)]))+"% výskyt)\n")
            procentamessage.append(min([round(intervaly_tachykardie/0.3), round(peak_velkeQRS/pocet_piku_procenta)]))

        if peak_velkeQRS > 0 and intervaly_arytmie > 0:
            textmessage.append("Komorové extrasystoly ("+str(min([round(intervaly_arytmie/0.3), round(peak_velkeQRS/pocet_piku_procenta)]))+"% výskyt)\n")
            procentamessage.append(min([round(intervaly_arytmie/0.3), round(peak_velkeQRS/pocet_piku_procenta)]))

        if peak_velkeP > 0:
            textmessage.append("P-mitrale ("+str(round(peak_velkeP/pocet_piku_procenta))+"% výskyt)\n")
            procentamessage.append(round(peak_velkeP/pocet_piku_procenta))

        if peak_velkeS > 0:
            textmessage.append("Blokáda pravého Tawarova raménka ("+str(round(peak_velkeS/pocet_piku_procenta))+"% výskyt)\n")
            procentamessage.append(round(peak_velkeS/pocet_piku_procenta))

        if peak_velkePxR > 0:
            textmessage.append("P-pulmonale ("+str(round(peak_velkePxR/pocet_piku_procenta))+"% výskyt)\n")
            procentamessage.append(round(peak_velkePxR/pocet_piku_procenta))

        if peak_malePR > 0: 
            textmessage.append("Preexcitace ("+str(round(peak_malePR/pocet_piku_procenta))+"% výskyt)\n")
            procentamessage.append(round(peak_malePR/pocet_piku_procenta))

        if peak_malePR > 0 and intervaly_tachykardie > 0:
            textmessage.append("WPW syndrom ("+str(round(peak_malePR/pocet_piku_procenta))+"% výskyt PR)\n")
            procentamessage.append(round(peak_malePR/pocet_piku_procenta))

        if peak_velkeQ > 0 or peak_velkeQxR > 0 or peak_velkeST > 0 or peak_maleST > 0:
            textmessage.append("Ischemická choroba srdeční ("+str(max([round(peak_velkeQ/pocet_piku_procenta), round(peak_velkeQxR/pocet_piku_procenta), round(peak_velkeST/pocet_piku_procenta), round(peak_maleST/pocet_piku_procenta)]))+"% výskyt)\n")
            procentamessage.append(max([round(peak_velkeQ/pocet_piku_procenta), round(peak_velkeQxR/pocet_piku_procenta), round(peak_velkeST/pocet_piku_procenta), round(peak_maleST/pocet_piku_procenta)]))

        if peak_velkeQTc > 0 or peak_maleQTc > 0:
            textmessage.append("Riziko vzniku nebezpečných komorových arytmií ("+str(round(peak_velkeQTc/pocet_piku_procenta)+round(peak_maleQTc/pocet_piku_procenta))+"% výskyt)\n")
            procentamessage.append(round(peak_velkeQTc/pocet_piku_procenta)+round(peak_maleQTc/pocet_piku_procenta))

        if peak_velkeTxR > 0:
            textmessage.append("Hyperkalémie nebo ischemická choroba srdeční ("+str(round(peak_velkeTxR/pocet_piku_procenta))+"% výskyt)\n")
            procentamessage.append(round(peak_velkeTxR/pocet_piku_procenta))


        sorted_indices = sorted(range(len(procentamessage)), key=lambda k: procentamessage[k], reverse=True)

        textms_final = ""
        for i in sorted_indices:
            textms_final += textmessage[i]

        self.statustextbox.configure(state="normal")
        self.statustextbox.insert("0.0", textms_final)
        self.statustextbox.configure(state="disabled")


        ###################################################################################################################################
        ################################################### ULOŽENÍ DAT DO EXCELU #########################################################
        ###################################################################################################################################
        self.statusbar_set(value=0.8)
        self.status_configure(text="Vytvářím Excelový soubor...")
        self.app.update()
        print("Vytvářím excel... ")
        
        wb = openpyxl.Workbook()
        ws = wb.active
        ws.title = "Data"

        if(self.chbx_export[0].get() == 1):
            ws["A1"] = "Čas [ms]"
            ws["B1"] = "Hodnota EKG [uV]"
            ws["C1"] = "Datarate:"
            ws["D1"] = str(datarate/100)
            
            for i in range(4):
                ws.cell(row=1, column = i+1).font = Font(bold = True)
            
            for i in range(len(x)):
                cell = ws.cell(row=i+2, column = 1)
                cell2 = ws.cell(row=i+2, column = 2)
                
                cell.value = x[i]
                cell2.value = y[i]
         

        ws1 = wb.create_sheet(title="Celkové výsledky")
        # vyhodnocení časové analýzy
        cislo_radku = 1
        ws1["A"+str(cislo_radku)] = "AVG (ms)"
        ws1["B"+str(cislo_radku)] = dif_avg
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "MIN (ms)"
        ws1["B"+str(cislo_radku)] = min_dif
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "MAX (ms)"
        ws1["B"+str(cislo_radku)] = max_dif

        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "NADev"
        ws1["B"+str(cislo_radku)] = NADev

        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "NADiff"
        ws1["B"+str(cislo_radku)] = NADiff

        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "SDNN (ms)"
        ws1["B"+str(cislo_radku)] = SDNN

        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "CV"
        ws1["B"+str(cislo_radku)] = CV
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "RMSSD (ms)"
        ws1["B"+str(cislo_radku)] = RMSSD

        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "nRMSSD"
        ws1["B"+str(cislo_radku)] = nRMSSD
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "pNN50 (%)"
        ws1["B"+str(cislo_radku)] = pNN50
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "MAD"
        ws1["B"+str(cislo_radku)] = MAD

        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "TPR"
        ws1["B"+str(cislo_radku)] = TPR

        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "HR (bpm)"
        ws1["B"+str(cislo_radku)] = bpm

        
        # vyhodnocení frekvenční analýzy
        cislo_radku += 2
        ws1["A"+str(cislo_radku)] = "VLF (ms2)"
        ws1["B"+str(cislo_radku)] = VLF
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "LF (ms2)"
        ws1["B"+str(cislo_radku)] = LF
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "HF (ms2)"
        ws1["B"+str(cislo_radku)] = HF
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "LF/HF"
        ws1["B"+str(cislo_radku)] = LF/HF
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "Total (ms2)"
        ws1["B"+str(cislo_radku)] = total_F

        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "VLF (%)"
        ws1["B"+str(cislo_radku)] = VLF_nu
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "LF (%)"
        ws1["B"+str(cislo_radku)] = LF_nu

        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "HF (%)"
        ws1["B"+str(cislo_radku)] = HF_nu


        # výsledky poincarého grafu
        cislo_radku += 2
        ws1["A"+str(cislo_radku)] = "PC_SD1 (ms)"
        ws1["B"+str(cislo_radku)] = sd1
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "PC_SD2 (ms)"
        ws1["B"+str(cislo_radku)] = sd2
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "PC_SD1/SD2"
        ws1["B"+str(cislo_radku)] = sd1/sd2

        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "PC_Plocha (ms2)"
        ws1["B"+str(cislo_radku)] = pc_plocha


        #všeobecné výsledky píků
        cislo_radku += 2
        ws1["A"+str(cislo_radku)] = "Počet pulzů"
        ws1["B"+str(cislo_radku)] = len(peaks)
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "Sinusové pulzy [%]"
        ws1["B"+str(cislo_radku)] = peak_sin/(len(peaks)/100)

        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "Chybějící pulzy [%]"
        ws1["B"+str(cislo_radku)] = peak_fibrilace/(len(peaks)/100)

        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "Nadbytečné pulzy [%]"
        ws1["B"+str(cislo_radku)] = peak_nadbytecne/(len(peaks)/100)

        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "OK pulzy"
        ws1["B"+str(cislo_radku)] = peak_OK/(len(peaks)/100)
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "Nevyhodnocené pulzy [%]"
        ws1["B"+str(cislo_radku)] = peak_nevyhodnoceno/(len(peaks)/100)
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = ">P [%]"
        ws1["B"+str(cislo_radku)] = peak_velkeP/(len(peaks)/100)
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "<PR [%]"
        ws1["B"+str(cislo_radku)] = peak_malePR/(len(peaks)/100)
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = ">PR [%]"
        ws1["B"+str(cislo_radku)] = peak_velkePR/(len(peaks)/100)

        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = ">P/R [%]"
        ws1["B"+str(cislo_radku)] = peak_velkePxR/(len(peaks)/100)
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = ">Q [%]"
        ws1["B"+str(cislo_radku)] = peak_velkeQ/(len(peaks)/100)
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = ">Q/R [%]"
        ws1["B"+str(cislo_radku)] = peak_velkeQxR/(len(peaks)/100)
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = ">S [%]"
        ws1["B"+str(cislo_radku)] = peak_velkeS/(len(peaks)/100)
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = ">QRS [%]"
        ws1["B"+str(cislo_radku)] = peak_velkeQRS/(len(peaks)/100)
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "<ST [%]"
        ws1["B"+str(cislo_radku)] = peak_maleST/(len(peaks)/100)
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = ">ST [%]"
        ws1["B"+str(cislo_radku)] = peak_velkeST/(len(peaks)/100)
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = ">T/R [%]"
        ws1["B"+str(cislo_radku)] = peak_velkeTxR/(len(peaks)/100)
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = ">QTc [%]"
        ws1["B"+str(cislo_radku)] = peak_velkeQTc/(len(peaks)/100)
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "<QTc [%]"
        ws1["B"+str(cislo_radku)] = peak_maleQTc/(len(peaks)/100)

        #všeobecné vcislo_radkuledky intervalů
        cislo_radku += 2
        ws1["A"+str(cislo_radku)] = "Počet intervalů arytmie [%]"
        ws1["B"+str(cislo_radku)] = intervaly_arytmie/0.3
        
        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "Počet intervalů bradykardie [%]"
        ws1["B"+str(cislo_radku)] = intervaly_bradykardie/0.3

        cislo_radku += 1
        ws1["A"+str(cislo_radku)] = "Počet intervalů tachykardie [%]"
        ws1["B"+str(cislo_radku)] = intervaly_tachykardie/0.3


        ws2 = wb.create_sheet(title="Pulzy")
        ws2["A1"] = "#"
        ws2["B1"] = "Time"
        ws2["C1"] = "RR"
        ws2["E1"] = "RR(x)"
        ws2["F1"] = "RR(x+1)"
        
        ws2["H1"] = "Pulz status"
        ws2["I1"] = "P [ms]"
        ws2["J1"] = "PR [ms]"
        ws2["K1"] = "P/R"
        ws2["L1"] = "Q [ms]"
        ws2["M1"] = "Q/R"
        ws2["N1"] = "R [ms]"
        ws2["O1"] = "S [ms]"
        ws2["P1"] = "QRS [ms]"
        ws2["Q1"] = "J/R"
        ws2["R1"] = "QT [ms]"
        ws2["S1"] = "QTc [ms]"
        ws2["T1"] = "T/R"

        for i in range(len(peaks)):
            ws2["A"+str(i+2)] = str(i+1)
            ws2["B"+str(i+2)] = str(peaks_in_ms[i])
            
            if(i == 0):
                ws2["C"+str(i+2)] = str(peaks_in_ms[i])
            else:
                ws2["C"+str(i+2)] = str(dif[i-1])

           
            ws2["H"+str(i+2)] = str(peak_hodnoceni[i].replace("_", " "))
            ws2["I"+str(i+2)] = str(Pduration[i])
            ws2["J"+str(i+2)] = str(PRinterval[i])
            ws2["K"+str(i+2)] = str(amplitudyPR[i])
            ws2["L"+str(i+2)] = str(Qduration[i])
            ws2["M"+str(i+2)] = str(amplitudyQR[i])
            ws2["N"+str(i+2)] = str(Rduration[i])
            ws2["O"+str(i+2)] = str(Sduration[i])
            ws2["P"+str(i+2)] = str(QRSduration[i])
            ws2["Q"+str(i+2)] = str(amplitudyJR[i])
            ws2["R"+str(i+2)] = str(QTduration[i])
            ws2["S"+str(i+2)] = str(QTcduration[i])
            ws2["T"+str(i+2)] = str(amplitudyTR[i])


        ws2["E"+str(2)] = peaks_in_ms[0]
        ws2["F"+str(2)] = dif[0]
        
        for j in range(len(dif)-1):
            ws2["E"+str(j+3)] = dif[j]
            plt2_x.append(dif[j])
            
            ws2["F"+str(j+3)] = dif[j+1]
            plt2_y.append(dif[j+1])
        
        
        
        ws2["V"+str(2)] = "Počet Pulzů"
        ws2["W"+str(2)] = len(peaks)
        
        ws2["V"+str(3)] = "Sinusové pulzy"
        ws2["W"+str(3)] = peak_sin

        ws2["V"+str(4)] = "Chybějící pulzy"
        ws2["W"+str(4)] = peak_fibrilace

        ws2["V"+str(4)] = "Nadbytečné pulzy"
        ws2["W"+str(4)] = peak_nadbytecne

        ws2["V"+str(6)] = "OK pulzy"
        ws2["W"+str(6)] = peak_OK
        
        ws2["V"+str(7)] = "Nevyhodnocené pulzy"
        ws2["W"+str(7)] = peak_nevyhodnoceno
        
        ws2["V"+str(8)] = ">P"
        ws2["W"+str(8)] = peak_velkeP
        
        ws2["V"+str(9)] = "<PR"
        ws2["W"+str(9)] = peak_malePR
        
        ws2["V"+str(10)] = ">PR"
        ws2["W"+str(10)] = peak_velkePR
        
        ws2["V"+str(11)] = ">P/R"
        ws2["W"+str(11)] = peak_velkePxR

        ws2["V"+str(12)] = ">Q"
        ws2["W"+str(12)] = peak_velkeQ
        
        ws2["V"+str(13)] = ">Q/R"
        ws2["W"+str(13)] = peak_velkeQxR

        ws2["V"+str(14)] = ">QRS"
        ws2["W"+str(14)] = peak_velkeQRS
        
        ws2["V"+str(15)] = "<ST"
        ws2["W"+str(15)] = peak_maleST
        
        ws2["V"+str(16)] = ">ST"
        ws2["W"+str(16)] = peak_velkeST
        
        ws2["V"+str(17)] = ">T/R"
        ws2["W"+str(17)] = peak_velkeTxR
        
        ws2["V"+str(18)] = ">QTc"
        ws2["W"+str(18)] = peak_velkeQTc
        
        ws2["V"+str(19)] = "<QTc"
        ws2["W"+str(19)] = peak_maleQTc
    
        
        #for i in range(19):
        #    ws2.cell(row=2+i, column = 8).font = Font(bold = True)
            
       
        ws3 = wb.create_sheet(title="Intervaly")
        ws3["A1"] = "#"
        ws3["B1"] = "RR AVG [ms]"
        ws3["C1"] = "RR MIN [ms]"
        ws3["D1"] = "RR MAX [ms]"
        ws3["E1"] = "HR [bpm]"
        ws3["F1"] = "NADev [ms]"
        ws3["G1"] = "NADiff [ms]"
        ws3["H1"] = "SDNN [ms]"
        ws3["I1"] = "CV"
        ws3["J1"] = "RMSSD [ms]"
        ws3["K1"] = "nRMSSD"
        ws3["L1"] = "pNN50 [%]"
        ws3["M1"] = "MAD"
        ws3["N1"] = "TPR"

        for i in range(len(interval_dif_avg)):
            ws3["A"+str(i+2)] = str(i+1)
            ws3["B"+str(i+2)] = interval_dif_avg[i]
            ws3["C"+str(i+2)] = interval_min_dif[i]
            ws3["D"+str(i+2)] = interval_max_dif[i]
            ws3["E"+str(i+2)] = interval_bpm[i]
            ws3["F"+str(i+2)] = interval_NADev[i]
            ws3["G"+str(i+2)] = interval_NADiff[i]
            ws3["H"+str(i+2)] = interval_SDNN[i]
            ws3["I"+str(i+2)] = interval_CV[i]
            ws3["J"+str(i+2)] = interval_RMSSD[i]
            ws3["K"+str(i+2)] = interval_nRMSSD[i]
            ws3["L"+str(i+2)] = interval_pNN50[i]
            ws3["M"+str(i+2)] = interval_MAD[i]
            ws3["N"+str(i+2)] = interval_TPR[i]
        
        ws3["P"+str(2)] = "Počet intervalů arytmie"
        ws3["Q"+str(2)] = intervaly_arytmie
        
        ws3["P"+str(3)] = "Počet intervalů bradykardie"
        ws3["Q"+str(3)] = intervaly_bradykardie

        ws3["P"+str(4)] = "Počet intervalů tachykardie"
        ws3["Q"+str(4)] = intervaly_tachykardie

        wb.save(slozka+"/"+slozka+".xlsx") # ulož excelový soubor do složky výsledků
        
        print("Hotovo")
        
        self.statusbar_set(value=1)
        self.status_configure(text="Vyhodnocení dokončeno :)")
        self.app.update()
        plt.show() # zobraz jediný graf se všemi hodnotami


    def statusbar_set(self,value):
        self.statusbar.set(value)
        self.statusbar2.set(value)

    def status_configure(self, text):
        self.status.configure(text=text)
        self.status2.configure(text=text)


    def filter_preview(self, cast_signalu, cisloPulzu):

        cisloPulzu = cisloPulzu-1 # Aby se intervaly a pulzy počítaly od 0

        hodnoty_x = []
        hodnoty_y = []
        i=0
        
        format_souboru = self.e_filename.get().split("/")
        format_souboru = format_souboru[len(format_souboru)-1]
        format_souboru = format_souboru.split(".")
        
        
        plt.style.use("default")
        
        if self.frame_soubory_main.get() == "Vyhodnocení ze souboru" and format_souboru[1] != "csv":
            filename = self.e_filename.get()
            
            f = open(filename, mode="rb")

            # Reading file data with read() method
            data = f.read()
            
            while(1):  
                try:
                    U = struct.unpack("<i",data[(i*4):((i*4)+4)]) 
                    #print(int(U[0]))
                    hodnoty_y.append(U[0]*0.001164153218269)
                    i=i+1
                except:
                    break
                
            datarate = U[0] # poslední řádek výsledkového souboru obsahuje datarate, ulož ho do jiné proměnné a smaž ho z měření
            
            
            hodnoty_y.remove(datarate*0.001164153218269)
            
            time_konst = 1/1.2 # ADS1262 měří 1200 vzorků za sekundu -> jeden řádek = 1/1.2 ms
            
            
            self.vzorkovaci_frekvence = 1200
            
            print("Rychlost vzorkování [Hz]: "+str(datarate/100))
            #self.l_datarate.configure(text=str(round(datarate/100))+" Hz")
            
            print(filename)
        else:
            # Jestliže je soubor naměřen pomocí EKG mini (výsledek se nemusí dekódovat z uint8_t)
            
            if self.frame_soubory_main.get() == "Měření EKG":
                filename = self.EKGmini_filename.get()+'.csv' #### self.EKGmini_filename.get()+"/"+
                
            elif format_souboru[1] == "csv":
                filename = self.e_filename.get()
                
            else:
                self.status_configure(text="Soubor error, prosím restart...")
            
            print(filename)
            f = open(filename, mode="rb")
            data = f.read()
            
            text_string = data.decode('utf-8') # Dokóduj byte string na klasický řetězec
            
            hodnoty_y = [int(match.group()) for match in re.finditer(r'\d+', text_string)] # Najdi všechna čísla v souboru a ulož je do pole
            
            time_konst = 1
            self.vzorkovaci_frekvence = 1000
            
            for i in range(len(hodnoty_y)):
                hodnoty_y[i] = (hodnoty_y[i]*950000)/3550

        
        for i in range(len(hodnoty_y)):
            hodnoty_x.append(i*time_konst) # vytvoř osu X s časovou značkou přepočítanou na milisekundy

        
        x = np.array(hodnoty_x) # ulož hodnoty měření do np.array proměnných
        y = np.array(hodnoty_y)


        
        desetsekund = self.vzorkovaci_frekvence *10
        filtered = y
        
        if(self.ch_50hzfiltr.get() == 1):
            b, a = signal.iirnotch(float(self.e_frekvence50hz.get()), float(self.e_rad50hz.get()),fs=self.vzorkovaci_frekvence)
            filtered = signal.filtfilt(b, a, filtered)
            
        
        if(self.ch_butterworth.get() == 1):
            if(self.e_typ.get() == "bandpass" or self.e_typ.get() == "bandstop"):
                freq = self.e_frekvence.get().split(",")
                sos = signal.butter(int(self.e_rad.get()),[float(freq[0]), float(freq[1])], btype=self.e_typ.get(),fs=self.vzorkovaci_frekvence, output="sos")
                filtered = signal.sosfiltfilt(sos, filtered)
            else:
                sos = signal.butter(int(self.e_rad.get()),float(self.e_frekvence.get()), btype=self.e_typ.get(),fs=self.vzorkovaci_frekvence, output="sos")
                filtered = signal.sosfiltfilt(sos, filtered)

        # NALEZENÍ R PÍKŮ
        peak_limit = int(self.e_peak_limit.get())*1000
        peaks, _ = signal.find_peaks(filtered, height=peak_limit, distance=150) # detekuj píky měření
        peaks_in_ms = [j * time_konst for j in peaks] # přepočítej časové hodnoty píků na milisekundy
        dif = np.diff(peaks_in_ms) # urči časový rozdíl mezi píky
        
        print(len(peaks))
        if(len(peaks) == 0):
            CTkMessagebox(title="Warning", message='Nebyly nalezeny žádné pulzy, upravte kritérium "Mez identifikace pulzů"', icon="warning") 

        elif(cast_signalu == 2 and cisloPulzu > len(peaks)):
            CTkMessagebox(title="Warning", message='Číslo vybraného pulzu je větší než celkový počet pulzů. Upravte políčko "Zobrazovaný pulz"', icon="warning") 

        elif(cast_signalu == 1 and cisloPulzu > 30):
            CTkMessagebox(title="Warning", message='Číslo vybraného intervalu je větší než celkový počet intervalů. Upravte políčko "Zobrazovaný interval"', icon="warning") 

        else:
            fig = plt.figure()
            fig.set_size_inches(16, 9)

            if cast_signalu == 0:    
                plt.title("Náhled filtrů signálu - celý signál")
                plt.xlabel("Čas [ms]")
                plt.ylabel("Napětí [uV]")
                
                plt.plot(x, y, label="Bez filtru")
                plt.plot(x, filtered, "r-", label="Filtrovaný záznam")
                plt.legend()
                plt.show()



            elif cast_signalu == 1:    
                l = 0
                while(peaks[l] < cisloPulzu*desetsekund):
                    if(l < len(peaks)-1):
                        l=l+1
                    else:
                        l=l+1
                        break
                
                firstpik = l
                print(firstpik)

                while(peaks[l] < (cisloPulzu+1)*desetsekund):
                    if(l < len(peaks)-1):
                        l=l+1
                    else:
                        l=l+1
                        break

                lastpik = l
                print(lastpik)
                plt.title("Náhled filtrů signálu - 10s interval č."+str(cisloPulzu+1))
                plt.xlabel("Čas [ms]")
                plt.ylabel("Napětí [uV]")
                plt.axhline(peak_limit, color="black", linestyle="dashdot", label ="Limit vyhodnocení pulzů")
                plt.plot(x[cisloPulzu*desetsekund:(cisloPulzu+1)*desetsekund], y[cisloPulzu*desetsekund:(cisloPulzu+1)*desetsekund], label="Bez filtru")
                plt.plot(x[cisloPulzu*desetsekund:(cisloPulzu+1)*desetsekund], filtered[cisloPulzu*desetsekund:(cisloPulzu+1)*desetsekund], "r-", label="Filtrovaný záznam")
                plt.plot(peaks_in_ms[firstpik:lastpik], filtered[peaks[firstpik:lastpik]], "x")

                print(filtered[peaks[firstpik:lastpik]])

                

                plt.legend()
                plt.show()

            elif cast_signalu == 2:
                plt.title("Náhled filtrů signálu - pulz č."+str(cisloPulzu+1))
                plt.xlabel("Čas [ms]")
                plt.ylabel("Napětí [uV]")
                
                i = cisloPulzu
                find_P_peaks, prominP = signal.find_peaks(filtered[peaks[i]-int(250):peaks[i]-int(60)], prominence=int(self.e_Pprominence.get()))
                find_T_peaks,prominT = signal.find_peaks(filtered[peaks[i]+int(60):peaks[i]+int(350)], prominence=int(self.e_Tprominence.get()))
                
                #prominences = signal.peak_prominences (filtered, find_P_peaks)[0]
                

                plt.plot(x[peaks[i]-500:peaks[i]+500], y[peaks[i]-500:peaks[i]+500], label="Bez filtru")
                plt.plot(x[peaks[i]-500:peaks[i]+500], filtered[peaks[i]-500:peaks[i]+500], "r-", label="Filtrovaný záznam")


                if(len(find_P_peaks) == 0):
                    CTkMessagebox(title="Warning", message='Nebyl nalezen žádný pulz P. Upravte políčko "Prominence P pulzů"', icon="warning") 
                else:
                    y_text_add = filtered[peaks[i]+int(60)+find_P_peaks[0]]/50
                    plt.plot(x[peaks[i]-int(250):peaks[i]-int(60)][find_P_peaks], filtered[peaks[i]-int(250):peaks[i]-int(60)][find_P_peaks], "x")
                    for j in range(len(prominP["prominences"])):
                        a = find_P_peaks[j]
                        plt.text(x[peaks[i]-int(250)+a], filtered[peaks[i]-int(250)+a]+y_text_add, str(round(prominP["prominences"][j])), fontsize = "large")
                

                if(len(find_T_peaks) == 0):
                    CTkMessagebox(title="Warning", message='Nebyl nalezen žádný pulz T. Upravte políčko "Prominence T pulzů"', icon="warning") 
                else:
                    y_text_add = filtered[peaks[i]+int(60)+find_T_peaks[0]]/50
                    plt.plot(x[peaks[i]+int(60):peaks[i]+int(350)][find_T_peaks], filtered[peaks[i]+int(60):peaks[i]+int(350)][find_T_peaks], "x")
                    for j in range(len(prominT["prominences"])):
                        plt.text(x[peaks[i]+int(60)+find_T_peaks[j]], filtered[peaks[i]+int(60)+find_T_peaks[j]]+y_text_add, str(round(prominT["prominences"][j])), fontsize = "large")

                plt.legend()
                plt.show()


        
    def read_ini(self):
        config = configparser.ConfigParser()
        config.read('EKG_analytik.ini')

        # SLOZKA
        self.e_filename.insert(0,config.get('SOUBOR', 'filename'))
        self.e_slozka.insert(0,config.get('SOUBOR', 'slozka'))
        
        # PARAMETRY - filtrace
        if config.get('FILTRACE', 'hz50filtr') == "1":
            self.ch_50hzfiltr.select()
        else:
            self.ch_50hzfiltr.deselect()
            
        if config.get('FILTRACE', 'butterworth') == "1":
            self.ch_butterworth.select()
        else:
            self.ch_butterworth.deselect()
        
        self.e_rad50hz.insert(0,config.get('FILTRACE', 'W_rad'))
        self.e_frekvence50hz.insert(0,config.get('FILTRACE', 'W_frekvence'))
        
        self.e_rad.insert(0,config.get('FILTRACE', 'B_rad'))
        self.e_frekvence.insert(0,config.get('FILTRACE', 'B_frekvence'))
        self.e_typ.set(config.get('FILTRACE', 'B_typ'))
        
        self.e_filter_cisloPulzu.insert(0,config.get('FILTRACE', 'filter_cisloIntervalu'))
        self.e_filter_cisloIntervalu.insert(0,config.get('FILTRACE', 'filter_cisloPulzu'))


        # PARAMETRY - Intervaly
        self.e_NADev.insert(0,config.get('ANALYZA_FS', 'NADev'))
        self.e_NADiff.insert(0,config.get('ANALYZA_FS', 'NADiff'))
        self.e_CV.insert(0,config.get('ANALYZA_FS', 'CV'))
        self.e_nRMSSD.insert(0,config.get('ANALYZA_FS', 'RMSSD'))
        self.e_MAD.insert(0,config.get('ANALYZA_FS', 'MAD'))
        self.e_TPR.insert(0,config.get('ANALYZA_FS', 'TPR'))
        #print(config.get('ZOBRAZIT'))

        # PARAMETRY - pulzy
        self.e_peak_limit .insert(0,config.get('PIKY', 'peak_limit'))
        self.e_Pprominence.insert(0,config.get('PIKY', 'prominence_P'))
        self.e_Tprominence.insert(0,config.get('PIKY', 'prominence_T'))

        self.e_PR_mez.insert(0,config.get('PIKY', 'mez_PR'))
        self.e_JR_mez.insert(0,config.get('PIKY', 'mez_JR'))
        self.e_TR_mez.insert(0,config.get('PIKY', 'mez_TR'))

        # ZOBRAZENÍ A EXPORT
        #zobrazit = dict(config.items('ZOBRAZIT'))
        export = dict(config.items('EXPORT'))
        
        j=0
        for i in export:
            if export[i] == "1":
                self.chbx_export[j].select()
            j=j+1

        # MĚŘENÍ EKG
        self.port = config.get("EKGmini", "port")
        self.monitor_speed_preset = config.get("EKGmini", "monitor_speed")
            
    def write_ini(self):
        config = configparser.ConfigParser()
        
        config.read('EKG_analytik.ini')
        config['SOUBOR'] = {'filename': self.e_filename.get(), 'slozka': self.e_slozka.get()}
        
        config['FILTRACE'] = {'HZ50filtr': self.ch_50hzfiltr.get(),'Butterworth': self.ch_butterworth.get(), 'W_Rad': self.e_rad50hz.get(), 'W_Frekvence': self.e_frekvence50hz.get(), 'B_Rad': self.e_rad.get(), 'B_Frekvence': self.e_frekvence.get(), 'B_Typ': self.e_typ.get(), 'filter_cisloIntervalu':self.e_filter_cisloIntervalu.get(), 'filter_cisloPulzu':self.e_filter_cisloPulzu.get()}
        config['ANALYZA_FS'] = {'NADev': self.e_NADev.get(), 'NADiff': self.e_NADiff.get(), 'CV': self.e_CV.get(), 'RMSSD': self.e_nRMSSD.get(), 'MAD': self.e_MAD.get(), 'TPR': self.e_TPR.get()}
        config['PIKY'] = {'peak_limit': self.e_peak_limit.get(), 'prominence_P':self.e_Pprominence.get(), 'prominence_T':self.e_Tprominence.get(), 'mez_PR': self.e_PR_mez.get(), 'mez_JR': self.e_JR_mez.get(), 'mez_TR': self.e_TR_mez.get()}

        #config['ZOBRAZIT'] = {'cely_graf': self.chbx_zobrazeni[0].get(),'intervaly10s': self.chbx_zobrazeni[1].get(),'piky': self.chbx_zobrazeni[2].get(), 'intervaly_rr': self.chbx_zobrazeni[3].get(),'pioncere': self.chbx_zobrazeni[4].get(),'welch': self.chbx_zobrazeni[5].get()}
        config['EXPORT'] = {'cely_graf': self.chbx_export[0].get(),'intervaly10s': self.chbx_export[1].get(),'piky': self.chbx_export[2].get(), 'intervaly_rr': self.chbx_export[3].get(),'pioncere': self.chbx_export[4].get(),'welch': self.chbx_export[5].get()}
        
        with open('EKG_analytik.ini', 'w') as configfile:
            config.write(configfile)
            print(config)
            
    
    def setup_window(self):
        def button_callback():
            filetypes = (('Data files', '*.dat *.csv *.json'),('All files', '*.*'))
            filename = askopenfilename(title='Open a file',filetypes=filetypes)
            self.e_filename.delete(0,ctk.END)
            self.e_filename.insert(0,filename)
            self.e_filename.xview_moveto(1.0)
            
            a = filename.split("/")
            a = a[len(a)-1]
            a = a.split(".")
            
            self.e_slozka.delete(0,ctk.END)
            self.e_slozka.insert(0,a[0])
            self.e_slozka.xview_moveto(1.0)
            

        master_frame = ctk.CTkFrame(master=self.app)
        master_frame.pack()
        nadpis = ctk.CTkLabel(master=master_frame, justify=ctk.LEFT, text="EKG ANALYTIK", font=("Futura", 40))
        nadpis.grid(row=1, column=1, columnspan=2, pady=(25,10), padx = (20,20))
        
        master_frame_l = ctk.CTkFrame(master=master_frame, fg_color="transparent")
        master_frame_l.grid(column=1,row=2, sticky="nesw", pady=10)
        master_frame_p = ctk.CTkFrame(master=master_frame, fg_color="transparent")
        master_frame_p.grid(column=2,row=2, sticky="nesw", pady=10)
        
        ############################################################################## FRAME SOUBORY #####################################################################################
        
        
        #frame_soubory = ctk.CTkFrame(master=master_frame)
        self.frame_soubory_main = ctk.CTkTabview(master=master_frame_l, fg_color="#333333") 
        
        frame_soubory = self.frame_soubory_main.add("Vyhodnocení ze souboru")
        EKGmini = self.frame_soubory_main.add("Měření EKG")
        self.frame_soubory_main.set("Vyhodnocení ze souboru")
        
        soubory_nadpis = ctk.CTkLabel(master=frame_soubory, justify=ctk.LEFT, text="SOUBORY", font=("Arial", 25))
        soubory_nadpis.pack(pady=(20,10))
        
        frame_in_soubory = ctk.CTkFrame(master=frame_soubory, fg_color="transparent")
        frame_in_soubory.pack(pady=5, padx=60, fill="both", expand=True)
        
        ctk.CTkLabel(master=frame_in_soubory, justify=ctk.LEFT, text="Název vstupního souboru:", font=("Arial", 15)).grid(row=1,column=1)
        
        self.e_filename = ctk.CTkEntry(master=frame_in_soubory, width=350)
        self.e_filename.grid(row=1,column=2, columnspan=2, padx=5, pady=5)
        
        b_browse = ctk.CTkButton(master=frame_in_soubory, command=button_callback, text="Procházet")
        b_browse.grid(row=1,column=5)
        
        
        ctk.CTkLabel(master=frame_in_soubory, justify=ctk.LEFT, text="Název výstupní složky:", font=("Arial", 15)).grid(row=2,column=1)
        
        self.e_slozka = ctk.CTkEntry(master=frame_in_soubory, placeholder_text="CTkEntry", width=350)
        self.e_slozka.grid(row=2,column=2, columnspan=2, padx=5, pady=5)
        
        self.statusbar = ctk.CTkProgressBar(master=frame_in_soubory, width=650, height=10)
        self.statusbar.grid(row=3, column=1, columnspan=5,pady=(30,10), padx=10, sticky="nesw")
        self.statusbar.set(value=0)
        
        
        ctk.CTkLabel(master=frame_in_soubory, justify=ctk.LEFT, text="Aktuální fáze programu:", font=("Arial", 15)).grid(row=4,column=1)
        self.status = ctk.CTkLabel(master=frame_in_soubory, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.status.grid(row=4,column=2)

        
        ### FRAME EKG mini
        soubory_nadpis = ctk.CTkLabel(master=EKGmini, justify=ctk.LEFT, text="MĚŘENÍ EKG", font=("Arial", 25))
        soubory_nadpis.pack(pady=(20,10))
        
        frame_in_ekg = ctk.CTkFrame(master=EKGmini, fg_color="transparent")
        frame_in_ekg.pack(pady=5, padx=60, fill="both", expand=True)
        
        ctk.CTkLabel(master=frame_in_ekg, justify=ctk.LEFT, text="Název výstupní složky a souboru:", font=("Arial", 15)).grid(row=1,column=1)
        
        self.EKGmini_filename = ctk.CTkEntry(master=frame_in_ekg, width=299)
        self.EKGmini_filename.grid(row=1,column=2, columnspan=2, padx=5, pady=5)
        
        b_browse = ctk.CTkButton(master=frame_in_ekg, command=self.EKGmini_init, text="Měření")
        b_browse.grid(row=1,column=5)
        
        self.statusbar2 = ctk.CTkProgressBar(master=frame_in_ekg, width=599, height=10)
        self.statusbar2.grid(row=2, column=0, columnspan=6,pady=(20,5),  sticky="nesw")
        self.statusbar2.set(value=0)
        

        ctk.CTkLabel(master=frame_in_ekg, justify=ctk.LEFT, text="Aktuální fáze programu:", font=("Arial", 15)).grid(row=3,column=1)
        self.status2 = ctk.CTkLabel(master=frame_in_ekg, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.status2.grid(row=3,column=2)

        self.EKG_delkamereni = ctk.CTkLabel(master=frame_in_ekg, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.EKG_delkamereni.grid(row=4,column=2)

        self.EKG_pocetradku = ctk.CTkLabel(master=frame_in_ekg, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.EKG_pocetradku.grid(row=5,column=2)
        
        ############################################################################## FRAME PARAMETRY #####################################################################################
        frame_parametry_main = ctk.CTkTabview(master = master_frame_l, fg_color="#333333") #ctk.CTkFrame(master=master_frame)
        frame_parametry = frame_parametry_main.add("Filtrace")
        frame_analyzaFS = frame_parametry_main.add("Intervaly")
        frame_piky = frame_parametry_main.add("Pulzy")

        parametry_nadpis = ctk.CTkLabel(master=frame_parametry, justify=ctk.LEFT, text="PARAMETRY VYHODNOCENÍ", font=("Arial", 25))
        parametry_nadpis.pack(pady=(20,10))
        
        frame_in_parametry = ctk.CTkFrame(master=frame_parametry, fg_color="transparent")
        frame_in_parametry.pack(pady=5, padx=50, fill="both", expand=True)

        

        f_50hz = ctk.CTkFrame(master=frame_in_parametry, fg_color="transparent")
        f_50hz.grid(row=1, column=1, columnspan=2, rowspan=3, sticky="nesw", pady=5, padx=(0,20))
        self.ch_50hzfiltr = ctk.CTkCheckBox(master=f_50hz, text= "Vrubový filtr")
        self.ch_50hzfiltr.grid(row=2,column=1, sticky="nesw")
       
        self.e_rad50hz = ctk.CTkEntry(master=f_50hz, justify=ctk.LEFT, placeholder_text="CTkEntry")
        self.e_rad50hz.grid(row=2,column=3, padx=5, sticky="nesw")
        
        self.e_frekvence50hz = ctk.CTkEntry(master=f_50hz, justify=ctk.LEFT, placeholder_text="CTkEntry")
        self.e_frekvence50hz.grid(row=3,column=3, padx=5, sticky="e") 
        
        ctk.CTkLabel(master=f_50hz, text="Q faktor:", font=("Arial", 12)).grid(row=2,column=2)
        ctk.CTkLabel(master=f_50hz, text="Frekvence [Hz]:", font=("Arial", 12)).grid(row=3,column=2)
        
        
        self.ch_butterworth = ctk.CTkCheckBox(master=f_50hz, text =" Filtr Butterworth")
        
        self.ch_butterworth.grid(row=5,column=1, sticky="nesw", pady=(10,0))
        
        self.e_rad = ctk.CTkEntry(master=f_50hz, justify=ctk.LEFT, placeholder_text="CTkEntry")
        self.e_rad.grid(row=5,column=3, padx=5, pady=(10,0))
        
        self.e_frekvence = ctk.CTkEntry(master=f_50hz, justify=ctk.LEFT, placeholder_text="CTkEntry")
        self.e_frekvence.grid(row=6,column=3, padx=5)
        
        self.e_typ = ctk.CTkComboBox(f_50hz, values=["lowpass", "highpass", "bandpass", "bandstop"])
        self.e_typ.grid(row=7,column=3, padx=5)
        self.e_typ.set("lowpass")
        

        
        ctk.CTkLabel(master=f_50hz, text="Řád: ", font=("Arial", 12)).grid(row=5,column=2, pady=(10,0))
        ctk.CTkLabel(master=f_50hz, text="Frekvence [Hz]:", font=("Arial", 12)).grid(row=6,column=2)
        ctk.CTkLabel(master=f_50hz, text="Typ:", font=("Arial", 12)).grid(row=7,column=2)
        

        f_zobrazit = ctk.CTkFrame(master=frame_in_parametry, fg_color="transparent")
        f_zobrazit.grid(row=1, column=6,sticky="nesw", pady=5)

        ctk.CTkButton(master=f_zobrazit, command=lambda: self.filter_preview(0,0), text="Zobrazit celé měření").grid(row=1,column=2, pady=(0,5))
        ctk.CTkButton(master=f_zobrazit, command=lambda: self.filter_preview(1,int(self.e_filter_cisloIntervalu.get())), text="Zobrazit 10s interval").grid(row=2,column=2, pady=(0,5))
        ctk.CTkButton(master=f_zobrazit, command=lambda: self.filter_preview(2,int(self.e_filter_cisloPulzu.get())), text="Zobrazit pulz").grid(row=3,column=2, pady=(0,5))
        
        ctk.CTkLabel(master=f_zobrazit, text="Zobrazovaný interval: ", font=("Arial", 15)).grid(row=4,column=1, pady=(5,0))
        ctk.CTkLabel(master=f_zobrazit, text="Zobrazovaný pulz:", font=("Arial", 15)).grid(row=5,column=1)

        self.e_filter_cisloIntervalu = ctk.CTkEntry(master=f_zobrazit, justify=ctk.LEFT, placeholder_text="5")
        self.e_filter_cisloIntervalu.grid(row=4,column=2, padx=5, pady=(5,0))


        self.e_filter_cisloPulzu = ctk.CTkEntry(master=f_zobrazit, justify=ctk.LEFT, placeholder_text="5")
        self.e_filter_cisloPulzu.grid(row=5,column=2, padx=5)
        

        ##################################################################### FRAME PARAMETRY - Analýza FS ##############################################################################
        parametry_nadpis = ctk.CTkLabel(master=frame_analyzaFS, justify=ctk.LEFT, text="PARAMETRY VYHODNOCENÍ", font=("Arial", 25))
        parametry_nadpis.pack(pady=(20,10))
        
        

        frame_in_analyza_fs = ctk.CTkFrame(master=frame_analyzaFS, fg_color="transparent")
        frame_in_analyza_fs.pack(pady=5, padx=50, fill="both", expand=True)
        ctk.CTkLabel(master=frame_in_analyza_fs, text="NADev: ",  font=("Arial", 15)).grid(row=1,column=1, pady=(10,0), padx=(54,0))
        ctk.CTkLabel(master=frame_in_analyza_fs, text="NADiff: ", font=("Arial", 15)).grid(row=2,column=1, pady=(10,0), padx=(54,0))
        ctk.CTkLabel(master=frame_in_analyza_fs, text="CV: ",     font=("Arial", 15)).grid(row=3,column=1, pady=(10,45), padx=(54,0))
        ctk.CTkLabel(master=frame_in_analyza_fs, text="nRMSSD: ",  font=("Arial", 15)).grid(row=1,column=3, pady=(10,0), padx=(50,0))
        ctk.CTkLabel(master=frame_in_analyza_fs, text="MAD: ",    font=("Arial", 15)).grid(row=2,column=3, pady=(10,0), padx=(50,0))
        ctk.CTkLabel(master=frame_in_analyza_fs, text="TPR: ",    font=("Arial", 15)).grid(row=3,column=3, pady=(10,45), padx=(50,0))

        self.e_NADev  = ctk.CTkEntry(master=frame_in_analyza_fs, justify=ctk.LEFT, placeholder_text="CTkEntry")
        self.e_NADiff = ctk.CTkEntry(master=frame_in_analyza_fs, justify=ctk.LEFT, placeholder_text="CTkEntry")
        self.e_CV     = ctk.CTkEntry(master=frame_in_analyza_fs, justify=ctk.LEFT, placeholder_text="CTkEntry")
        self.e_nRMSSD  = ctk.CTkEntry(master=frame_in_analyza_fs, justify=ctk.LEFT, placeholder_text="CTkEntry")
        self.e_MAD    = ctk.CTkEntry(master=frame_in_analyza_fs, justify=ctk.LEFT, placeholder_text="CTkEntry")
        self.e_TPR    = ctk.CTkEntry(master=frame_in_analyza_fs, justify=ctk.LEFT, placeholder_text="CTkEntry")
        

        self.e_NADev .grid(row=1,column=2, pady=(10,0), padx=(0,20))
        self.e_NADiff.grid(row=2,column=2, pady=(10,0), padx=(0,20))
        self.e_CV    .grid(row=3,column=2, pady=(10,45), padx=(0,20))
        self.e_nRMSSD .grid(row=1,column=4, pady=(10,0), padx=(0,53))
        self.e_MAD   .grid(row=2,column=4, pady=(10,0), padx=(0,53))
        self.e_TPR   .grid(row=3,column=4, pady=(10,45), padx=(0,53))


        ##################################################################### FRAME PARAMETRY - Pulzy ##############################################################################
        parametry_nadpis = ctk.CTkLabel(master=frame_piky, justify=ctk.LEFT, text="PARAMETRY VYHODNOCENÍ", font=("Arial", 25))
        parametry_nadpis.pack(pady=(20,10))
        
        frame_in_piky = ctk.CTkFrame(master=frame_piky, fg_color="transparent")
        frame_in_piky.pack( padx=50, pady=(5,55), fill="both", expand=True)
        
        # Mez identifikace píků
        ctk.CTkLabel(master=frame_in_piky, text="Mez identifikace pulzů [mV]:", font=("Arial", 15)).grid(row=1,column=1, pady=(10,0))
        
        self.e_peak_limit = ctk.CTkEntry(master=frame_in_piky, justify=ctk.LEFT, placeholder_text="CTkEntry")
        self.e_peak_limit.grid(row=1,column=2, padx=(5,20), pady=(10,0))

        # Prominence P a T píků
        ctk.CTkLabel(master=frame_in_piky, text="Prominence P pulzů: ", font=("Arial", 15)).grid(row=2,column=1, pady=(10,0))
        ctk.CTkLabel(master=frame_in_piky, text="Prominence T pulzů: ", font=("Arial", 15)).grid(row=3,column=1, pady=(0,0))

        self.e_Pprominence = ctk.CTkEntry(master=frame_in_piky, justify=ctk.LEFT, placeholder_text="CTkEntry")
        self.e_Pprominence.grid(row=2,column=2, padx=(5,20), pady=(10,0))

        self.e_Tprominence = ctk.CTkEntry(master=frame_in_piky, justify=ctk.LEFT, placeholder_text="CTkEntry")
        self.e_Tprominence.grid(row=3,column=2, padx=(5,20), pady=(10,0))


        # Mezní hodnoty P/R, J/R a T/R
        ctk.CTkLabel(master=frame_in_piky, text="Mezní hodnota P/R: ", font=("Arial", 15)).grid(row=1,column=3, pady=(10,0))
        ctk.CTkLabel(master=frame_in_piky, text="Mezní hodnota J/R: ", font=("Arial", 15)).grid(row=2,column=3, pady=(10,0))
        ctk.CTkLabel(master=frame_in_piky, text="Mezní hodnota T/R: ", font=("Arial", 15)).grid(row=3,column=3, pady=(10,0))

        self.e_PR_mez = ctk.CTkEntry(master=frame_in_piky, justify=ctk.LEFT, placeholder_text="CTkEntry")
        self.e_PR_mez.grid(row=1,column=4, padx=5, pady=(10,0))

        self.e_JR_mez = ctk.CTkEntry(master=frame_in_piky, justify=ctk.LEFT, placeholder_text="CTkEntry")
        self.e_JR_mez.grid(row=2,column=4, padx=5, pady=(10,0))

        self.e_TR_mez = ctk.CTkEntry(master=frame_in_piky, justify=ctk.LEFT, placeholder_text="CTkEntry")
        self.e_TR_mez.grid(row=3,column=4, padx=5, pady=(10,0))


        ############################################################################## FRAME EXPORT #####################################################################################
        frame_zobrazeni = ctk.CTkFrame(master=master_frame_l, fg_color="#333333")

        zobrazeni_nadpis = ctk.CTkLabel(master=frame_zobrazeni, justify=ctk.LEFT, text="EXPORT", font=("Arial", 25))
        zobrazeni_nadpis.pack(pady=(20,10))
        
        frame_in_zobrazeni = ctk.CTkFrame(master=frame_zobrazeni, fg_color="transparent")
        frame_in_zobrazeni.pack(pady=5, padx=60, fill="both", expand=True)
        
        #ctk.CTkLabel(master=frame_in_zobrazeni, justify=ctk.LEFT, text="Zobrazit naměřený graf", font=("Arial", 15)).grid(row=1,column=2, padx = 20)
        ctk.CTkLabel(master=frame_in_zobrazeni, justify=ctk.LEFT, text="Exportovat do souborů", font=("Arial", 15)).grid(row=1,column=3, padx=(140,0))
        
        ctk.CTkLabel(master=frame_in_zobrazeni, justify=ctk.LEFT, text="Celkový graf měření:", font=("Arial", 15)).grid(row=2,column=1)
        ctk.CTkLabel(master=frame_in_zobrazeni, justify=ctk.LEFT, text="10s intervaly:", font=("Arial", 15)).grid(row=3,column=1)
        ctk.CTkLabel(master=frame_in_zobrazeni, justify=ctk.LEFT, text="Jednotlivé pulzy:", font=("Arial", 15)).grid(row=4,column=1)
        ctk.CTkLabel(master=frame_in_zobrazeni, justify=ctk.LEFT, text="Intervaly RR:", font=("Arial", 15)).grid(row=5,column=1)
        ctk.CTkLabel(master=frame_in_zobrazeni, justify=ctk.LEFT, text="Poincarého graf:", font=("Arial", 15)).grid(row=6,column=1)
        ctk.CTkLabel(master=frame_in_zobrazeni, justify=ctk.LEFT, text="Frekvenční analýza:", font=("Arial", 15)).grid(row=7,column=1, pady=(0,30))
        
        #self.chbx_zobrazeni = []
        self.chbx_export = []
        for i in range(6):
            #self.chbx_zobrazeni.append(ctk.CTkCheckBox(master=frame_in_zobrazeni, text= ""))
            #self.chbx_zobrazeni[i].grid(row=2+i,column=2, sticky="ne")
            
            self.chbx_export.append(ctk.CTkCheckBox(master=frame_in_zobrazeni, text= ""))
            self.chbx_export[i].grid(row=2+i,column=3, sticky="ne")
        
      
        ############################################################################## FRAME PARAMETRY MĚŘENÍ #####################################################################################
        """
        frame_parametry_m = ctk.CTkFrame(master=master_frame, width=500)
        

        parametry_m_nadpis = ctk.CTkLabel(master=frame_parametry_m, justify=ctk.LEFT, text="PARAMETRY MĚŘENÍ", font=("Arial", 25))
        parametry_m_nadpis.pack(pady=(20,10))
        
        frame_in_parametry_m = ctk.CTkFrame(master=frame_parametry_m, fg_color="transparent")
        frame_in_parametry_m.pack(pady=5, padx=60, fill="both", expand=True)
        
        ctk.CTkLabel(master=frame_in_parametry_m, justify=ctk.LEFT, text="", font=("Arial", 5), width=670).grid(row=4,column=1, columnspan=3)
        
        ctk.CTkLabel(master=frame_in_parametry_m, justify=ctk.LEFT, text="Rychlost vzorkování:", font=("Arial", 15)).grid(row=1,column=1)
        self.l_datarate = ctk.CTkLabel(master=frame_in_parametry_m, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_datarate.grid(row=1,column=2)
        
        ctk.CTkLabel(master=frame_in_parametry_m, justify=ctk.LEFT, text="Počet řádků datového souboru:", font=("Arial", 15)).grid(row=2,column=1)
        self.l_radky_souboru = ctk.CTkLabel(master=frame_in_parametry_m, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_radky_souboru.grid(row=2,column=2)
        
        
        
        ctk.CTkLabel(master=frame_in_parametry_m, justify=ctk.LEFT, text="Počet identifikovaných pulzů:", font=("Arial", 15)).grid(row=3,column=1)
        self.l_pocet_piku = ctk.CTkLabel(master=frame_in_parametry_m, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_pocet_piku.grid(row=3,column=2)
        
        """
        ############################################################################## VÝSLEDKY #####################################################################################
        frame_vysledky = ctk.CTkFrame(master=master_frame_p, fg_color="#333333")
        status_nadpis = ctk.CTkLabel(master=frame_vysledky, justify=ctk.LEFT, text="SOUHRNNÉ VÝSLEDKY MĚŘENÍ", font=("Arial", 25))
        status_nadpis.pack(pady=(20,10))
        
        frame_vysledky2 = ctk.CTkFrame(master=frame_vysledky, fg_color="transparent")
        frame_vysledky2.pack(side=ctk.TOP, fill="both", expand=True)
        
        frame_in_vysledky = ctk.CTkFrame(master=frame_vysledky2, fg_color="transparent")
        frame_in_vysledky.grid(row=1, column=1,pady=(5,0), padx=20, sticky="new")
        
        ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="Časová analýza", width=200, font=("Arial", 20)).grid(row=0,column=1, columnspan=2, pady=(5,15))

        ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="RR avg / HR: ", font=("Arial", 15)).grid(row=1,column=1, sticky="e")
        self.l_RRavg_bpm = ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="                               ", font=("Arial", 15))
        self.l_RRavg_bpm.grid(row=1,column=2) # l_avg_dif
        
        ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="RR min / max: ", font=("Arial", 15)).grid(row=2,column=1, sticky="e")
        self.l_RRmin_max = ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_RRmin_max.grid(row=2,column=2) #l_min_dif
        
        ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="NADev: ", font=("Arial", 15)).grid(row=3,column=1, sticky="e")
        self.l_NADev = ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_NADev.grid(row=3,column=2)

        ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="NADiff: ", font=("Arial", 15)).grid(row=4,column=1, sticky="e")
        self.l_NADiff = ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_NADiff.grid(row=4,column=2)
        
        ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="SDNN / CV: ", font=("Arial", 15)).grid(row=5,column=1, sticky="e")
        self.l_sdnn_cv = ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_sdnn_cv.grid(row=5,column=2)
        
        ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="RMSSD / nRMSSD: ", font=("Arial", 15)).grid(row=6,column=1, sticky="e")
        self.l_rmssd = ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_rmssd.grid(row=6,column=2)
        
        ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="pNN50: ", font=("Arial", 15)).grid(row=7,column=1, sticky="e")
        self.l_pnn50 = ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_pnn50.grid(row=7,column=2)
        

        ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="MAD: ", font=("Arial", 15)).grid(row=8,column=1, sticky="e")
        self.l_MAD = ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_MAD.grid(row=8,column=2)

        ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="TPR: ", font=("Arial", 15)).grid(row=9,column=1, sticky="e")
        self.l_TPR = ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_TPR.grid(row=9,column=2)
        

        frame_in_vysledky2 = ctk.CTkFrame(master=frame_vysledky2, fg_color="transparent")
        frame_in_vysledky2.grid(row=1, column=2,pady=5, padx=40, sticky = "new")
        
        ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="Frekvenční analýza", width=200, font=("Arial", 20)).grid(row=0,column=1, columnspan=2, pady=(5,15))
        
        ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="VLF:", font=("Arial", 15)).grid(row=1,column=1, sticky="e")
        self.l_vlf = ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_vlf.grid(row=1,column=2)
        
        ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="LF:", font=("Arial", 15)).grid(row=2,column=1, sticky="e")
        self.l_lf = ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_lf.grid(row=2,column=2)
        
        ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="HF:", font=("Arial", 15)).grid(row=3,column=1, sticky="e")
        self.l_hf = ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_hf.grid(row=3,column=2)
        

        ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="Total:", font=("Arial", 15)).grid(row=4,column=1, sticky="e")
        self.l_f_total = ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_f_total.grid(row=4,column=2)

        ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="LF/HF:", font=("Arial", 15)).grid(row=5,column=1, sticky="e")
        self.l_lfhf = ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_lfhf.grid(row=5,column=2)
        
        

        ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="VLF:", font=("Arial", 15)).grid(row=6,column=1, sticky="e")
        self.l_vlf_n = ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_vlf_n.grid(row=6,column=2)
        
        ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="LF:", font=("Arial", 15)).grid(row=7,column=1, sticky="e")
        self.l_lf_n = ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_lf_n.grid(row=7,column=2)
        
        ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="HF:", font=("Arial", 15)).grid(row=8,column=1, sticky="e")
        self.l_hf_n = ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_hf_n.grid(row=8,column=2)

        
        
        frame_in_vysledky3 = ctk.CTkFrame(master=frame_vysledky2, fg_color="transparent")
        frame_in_vysledky3.grid(row=1, column=3,pady=5, padx=20, sticky="new")
        
        ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="Poincarého graf", width=200, font=("Arial", 20)).grid(row=0,column=1, columnspan=2, pady=(5,15))
        
        ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="SD1: ", font=("Arial", 15)).grid(row=1,column=1, sticky="e")
        self.l_sd1 = ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_sd1.grid(row=1,column=2)
        
        ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="SD2: ", font=("Arial", 15)).grid(row=2,column=1, sticky="e")
        self.l_sd2 = ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_sd2.grid(row=2,column=2)
        
        ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="SD1/SD2: ", font=("Arial", 15)).grid(row=3,column=1, sticky="e")
        self.l_sd1sd2 = ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_sd1sd2.grid(row=3,column=2)

        ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="S: ", font=("Arial", 15)).grid(row=4,column=1, sticky="e")
        self.l_pc_plocha = ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_pc_plocha.grid(row=4,column=2)
        

        ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="Analýza 10s intervalů", width=200, font=("Arial", 20)).grid(row=5,column=1, columnspan=2, pady=(15,5))

        ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="Intervalů arytmie:", font=("Arial", 15)).grid(row=6,column=1, sticky="ne")
        self.l_intervaly_arytmie = ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_intervaly_arytmie.grid(row=6,column=2)

        ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="Intervalů bradykardie:", font=("Arial", 15)).grid(row=7,column=1, sticky="ne")
        self.l_intervaly_bradykardie = ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_intervaly_bradykardie.grid(row=7,column=2)

        ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="Intervalů tachykardie:", font=("Arial", 15)).grid(row=8,column=1, sticky="ne")
        self.l_intervaly_tachykardie = ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_intervaly_tachykardie.grid(row=8,column=2)


        ctk.CTkLabel(master=frame_in_vysledky, justify=ctk.LEFT, text="",width=200, height=1, font=("Arial", 1)).grid(row=10,column=1, columnspan=3)
        ctk.CTkLabel(master=frame_in_vysledky2, justify=ctk.LEFT, text="",width=200, height=1,font=("Arial", 1)).grid(row=10,column=1, columnspan=3)
        ctk.CTkLabel(master=frame_in_vysledky3, justify=ctk.LEFT, text="",width=200, height=1,font=("Arial", 1)).grid(row=10,column=1, columnspan=3)

        #################################################################################################################
        
        frame_vyhodnoceni_pulzu = ctk.CTkFrame(master=master_frame_p, fg_color="#333333", width=500)
        

        frame_vyhodnoceni_pulzu_nadpis = ctk.CTkLabel(master=frame_vyhodnoceni_pulzu, justify=ctk.LEFT, text="VYHODNOCENÍ PULZŮ", font=("Arial", 25))
        frame_vyhodnoceni_pulzu_nadpis.pack(pady=(20,10))
        
        frame_in_vyhodnoceni_pulzu = ctk.CTkFrame(master=frame_vyhodnoceni_pulzu, fg_color="transparent")
        frame_in_vyhodnoceni_pulzu.pack(pady=5, padx=50, fill="x", expand=True)

        
        
        frame_in_vyhodnoceni_pulzu1 = ctk.CTkFrame(master=frame_in_vyhodnoceni_pulzu, fg_color="transparent")
        frame_in_vyhodnoceni_pulzu1.grid(row=1, column=1,pady=5, padx=20, sticky='NSEW')

        frame_in_vyhodnoceni_pulzu2 = ctk.CTkFrame(master=frame_in_vyhodnoceni_pulzu, fg_color="transparent")
        frame_in_vyhodnoceni_pulzu2.grid(row=1, column=2,pady=5, padx=20, sticky='NSEW')

        frame_in_vyhodnoceni_pulzu3 = ctk.CTkFrame(master=frame_in_vyhodnoceni_pulzu, fg_color="transparent")
        frame_in_vyhodnoceni_pulzu3.grid(row=1, column=3,pady=5, padx=20, sticky='NSEW')

        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu1, justify=ctk.LEFT, text="Celkem pulzů: ", font=("Arial", 15)).grid(row=1,column=1, sticky="e")
        self.l_pocet_piku = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu1, justify=ctk.LEFT, text="       ", font=("Arial", 15))
        self.l_pocet_piku.grid(row=1,column=2)

        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu1, justify=ctk.LEFT, text="Sinusové pulzy: ", font=("Arial", 15)).grid(row=2,column=1, sticky="e")
        self.l_piky_sin = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu1, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_piky_sin.grid(row=2,column=2)

        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu1, justify=ctk.LEFT, text="Chybějící pulzy: ", font=("Arial", 15)).grid(row=3,column=1, sticky="e")
        self.l_piky_fibrilacni = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu1, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_piky_fibrilacni.grid(row=3,column=2)

        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu1, justify=ctk.LEFT, text="Nadbytečné pulzy: ", font=("Arial", 15)).grid(row=4,column=1, sticky="e")
        self.l_piky_nadbytecne = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu1, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_piky_nadbytecne.grid(row=4,column=2)
        
        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu1, justify=ctk.LEFT, text="Nevyhodnoceno: ", font=("Arial", 15)).grid(row=5,column=1, sticky="e")
        self.l_piky_nevyhodnoceno = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu1, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_piky_nevyhodnoceno.grid(row=5,column=2)

        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu1, justify=ctk.LEFT, text="OK pulzy: ", font=("Arial", 15)).grid(row=6,column=1, sticky="e")
        self.l_ok_piky = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu1, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_ok_piky.grid(row=6,column=2)


        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu2, justify=ctk.LEFT, text=">P: ", font=("Arial", 15)).grid(row=1,column=1, sticky="e")
        self.l_piky_velkeP = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu2, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_piky_velkeP.grid(row=1,column=2)

        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu2, justify=ctk.LEFT, text="<PR: ", font=("Arial", 15)).grid(row=2,column=1, sticky="e")
        self.l_piky_malePR = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu2, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_piky_malePR.grid(row=2,column=2)

        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu2, justify=ctk.LEFT, text=">PR: ", font=("Arial", 15)).grid(row=3,column=1, sticky="e")
        self.l_piky_velkePR = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu2, justify=ctk.LEFT, text="         ", font=("Arial", 15))
        self.l_piky_velkePR.grid(row=3,column=2)

        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu2, justify=ctk.LEFT, text=">P/R: ", font=("Arial", 15)).grid(row=4,column=1, sticky="e")
        self.l_piky_velkePxR = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu2, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_piky_velkePxR.grid(row=4,column=2)

        

        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu2, justify=ctk.LEFT, text=">Q: ", font=("Arial", 15)).grid(row=5,column=1, sticky="e")
        self.l_piky_velkeQ = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu2, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_piky_velkeQ.grid(row=5,column=2)

        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu2, justify=ctk.LEFT, text=">Q/R: ", font=("Arial", 15)).grid(row=6,column=1, sticky="e")
        self.l_piky_velkeQxR = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu2, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_piky_velkeQxR.grid(row=6,column=2)
        
        



        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu2, justify=ctk.LEFT, text=">QRS: ", font=("Arial", 15)).grid(row=7,column=1, sticky="e")
        self.l_piky_velkeQRS = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu2, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_piky_velkeQRS.grid(row=7,column=2)

        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu3, justify=ctk.LEFT, text=">S: ", font=("Arial", 15)).grid(row=1,column=1, sticky="e")
        self.l_piky_velkeS = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu3, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_piky_velkeS.grid(row=1,column=2)
        
        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu3, justify=ctk.LEFT, text="<ST: ", font=("Arial", 15)).grid(row=2,column=1, sticky="e")
        self.l_piky_maleST = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu3, justify=ctk.LEFT, text="          ", font=("Arial", 15))
        self.l_piky_maleST.grid(row=2,column=2)

        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu3, justify=ctk.LEFT, text=">ST: ", font=("Arial", 15)).grid(row=3,column=1, sticky="e")
        self.l_piky_velkeST = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu3, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_piky_velkeST.grid(row=3,column=2)

        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu3, justify=ctk.LEFT, text=">T/R: ", font=("Arial", 15)).grid(row=4,column=1, sticky="e")
        self.l_piky_velkeTxR = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu3, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_piky_velkeTxR.grid(row=4,column=2)

        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu3, justify=ctk.LEFT, text=">QTc: ", font=("Arial", 15)).grid(row=5,column=1, sticky="e")
        self.l_piky_velkeQTc = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu3, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_piky_velkeQTc.grid(row=5,column=2)

        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu3, justify=ctk.LEFT, text="<QTc: ", font=("Arial", 15)).grid(row=6,column=1, sticky="e")
        self.l_piky_maleQTc = ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu3, justify=ctk.LEFT, text="", font=("Arial", 15))
        self.l_piky_maleQTc.grid(row=6,column=2)


        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu1, justify=ctk.LEFT, text="",width=200, height=1, font=("Arial", 1)).grid(row=10,column=1, columnspan=3)
        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu2, justify=ctk.LEFT, text="",width=200, height=1,font=("Arial", 1)).grid(row=10,column=1, columnspan=3)
        ctk.CTkLabel(master=frame_in_vyhodnoceni_pulzu3, justify=ctk.LEFT, text="",width=200, height=1,font=("Arial", 1)).grid(row=10,column=1, columnspan=3)

        frame_vyhodnoceni_statusbox = ctk.CTkFrame(master=master_frame_p, fg_color="#333333")
        
        statusbox_nadpis = ctk.CTkLabel(master=frame_vyhodnoceni_statusbox, justify=ctk.LEFT, text="CELKOVÉ VYHODNOCENÍ", font=("Arial", 25))
        statusbox_nadpis.pack(pady=(20,10))

        self.statustextbox = ctk.CTkTextbox(master=frame_vyhodnoceni_statusbox, font=("Arial", 15), height=120)
        self.statustextbox.configure(state="disabled")
        self.statustextbox.pack(side=ctk.TOP, fill="x", expand="True", pady=10, padx=20)

        


        #################################################################################################################
        self.frame_soubory_main.grid(row=2, column=1,pady=5, padx=30, sticky="nsew")
        frame_parametry_main.grid(row=3, column=1,pady=5, padx=30, sticky="nsew", rowspan=4)
        frame_zobrazeni   .grid(row=7, column=1,pady=(20,20), padx=30, sticky="nsew")
        
        #frame_parametry_m .grid(row=2, column=2,pady=(20,40), padx=50, sticky="nsew")
        frame_vysledky    .grid(row=2, column=2,pady=(20,10), padx=30, sticky="nsew")
        frame_vyhodnoceni_pulzu.grid(row=5, column=2,pady=(10,10), padx=30, sticky="nsew")
        frame_vyhodnoceni_statusbox.grid(row=8, column=2,pady=(10,20), padx=30, sticky="nsew") #.pack(pady=5, padx=50, fill="x", expand=True)

        ctk.CTkButton(master=frame_zobrazeni, command=self.mereni, text="Začít vyhodnocení").place(relx=0.95, rely=0.85, anchor='ne')#.grid(row=7, column=2,pady = (0,20))
        
        ctk.CTkButton(master=frame_zobrazeni, command=self.write_ini, text="Zapsat .INI").place(relx=0.95, rely=0.09, anchor='ne') #.grid(row=7, column=1,pady = (0,20))
        
        
        def ask_question():
            # get yes/no answers
            msg = CTkMessagebox(title="Ukončit?", message="Opravdu chcete ukončit program?",
                                icon="question", option_1="Zrušit", option_2="Ne", option_3="Ano")
            response = msg.get()
            
            if response=="Ano":
                self.app.destroy()       
                plt.close(1)
                sys.exit("Program ukončen")
            
        
        ctk.CTkButton(master=master_frame, command=ask_question, text="Ukončit", fg_color="red").place(relx=1, rely=0, anchor='ne')
        
        
        self.read_ini()
        
        self.app.mainloop()
        
        
        
    def EKGmini_init(self):
        self.EKG_window = ctk.CTkToplevel(self.app)
        self.height = 1080
        self.width = 1920
        self.EKG_window.title("EKG mini")
        self.EKG_window.geometry("1920x1080")
        self.previewbool = False
        self.measurebool = False
        self.merenibool = False
        self.countdownbool = False
        

        plt.style.use({
            'axes.facecolor': '#242424',
            'axes.edgecolor': 'gray',
            'axes.labelcolor': 'white',
            'text.color': 'white',
            'xtick.color': 'white',
            'ytick.color': 'white',
            'grid.color': 'gray',
            'figure.facecolor': '#242424',
            'figure.edgecolor': '#242424',
            'savefig.facecolor': '#242424',
            'savefig.edgecolor': '#242424',
        })
        
        
        

        self.chartframe = ctk.CTkFrame(self.EKG_window, width = self.width, height=self.height*0.87, fg_color=("red"))
        self.chartframe.grid(row=2, column =0,columnspan = 20, sticky="NESW", pady = (20,10), padx=(self.width*0.005))
        
            
        fig = plt.Figure(figsize=(19, 9.25))
        self.ax = fig.add_subplot(111)
        #self.ax.plot([1,2,3,4],[1,2,3,4])
        
        self.canvas = FigureCanvasTkAgg(fig, master=self.chartframe)  # A tk.DrawingArea.
        self.canvas.draw()

        self.canvas.get_tk_widget().pack()
        
        #self.toolbar = NavigationToolbar2Tk(self.canvas, self.chartframe)
        #self.toolbar.update()
        #toolbar.grid(row=1, column =0)

        ctk.CTkLabel(self.EKG_window, text="", width=self.width).grid(row=3, column = 0,columnspan = 20, sticky="nesw")
        
        ctk.CTkLabel(self.EKG_window, text="Počet řádků; čas měření [ms]; čas přenosu dat [ms]: ").place(x=260, y=220)#.grid(row=0, column = 2) # , sticky="nesw"
        self.previewstatus_l = ctk.CTkLabel(self.EKG_window, text="")
        self.previewstatus_l.place(x=570, y=220)#.grid(row=0, column = 3) # , sticky="nesw"
                
        #updatebutton = ctk.CTkButton(self.EKG_window, text="Náhled", command=self.start_preview)
        #updatebutton.grid(row=0, column = 2) # , sticky="nesw"
        
        startbutton = ctk.CTkButton(self.EKG_window, text="Změřit", command=self.start_measure)
        startbutton.grid(row=0, column = 2) # , sticky="nesw"
        
        port_toplevel = ctk.CTkToplevel(self.app)
        port_toplevel.geometry("300x100")
        port_toplevel.attributes('-topmost','true')
        port_toplevel.title("Vyber port zařízení")
        
        ctk.CTkLabel(port_toplevel, text="Port: ", font=("Futura", 15)).grid(row=0, column = 0,sticky="nesw") # , sticky="nesw"
        self.e_port = ctk.CTkComboBox(port_toplevel, values=["/dev/ttyUSB0", "/dev/ttyUSB1", "/dev/ttyUSB3"])
        self.e_port.grid(row=0, column = 1 , sticky="nesw") # , sticky="nesw"
        self.e_port.set((self.port))
        
        ctk.CTkLabel(port_toplevel, text="Rychlost přenosu: ", font=("Futura", 15)).grid(row=1, column = 0,sticky="nesw") # , sticky="nesw"
        self.e_monitorspeed = ctk.CTkComboBox(port_toplevel, values=["115200", "576000", "1000000", "1152000", "2000000"])
        self.e_monitorspeed.grid(row=1, column = 1 , sticky="nesw")
        self.e_monitorspeed.set(str(self.monitor_speed_preset))
        
        
        self.ax.set_xlabel("Čas [ms]")
        self.ax.set_ylabel("Napětí [mV]")
        
        def submit_port():
            self.port = self.e_port.get()
            
            
            #   write config
            config = configparser.ConfigParser()

            config.read('EKG_analytik.ini')
            config.set('EKGmini', 'port',self.e_port.get())
            config.set('EKGmini', 'monitor_speed',self.e_monitorspeed.get())
            
            with open('EKG_analytik.ini', 'w') as configfile:
                config.write(configfile)
            
            print(self.port)

            
            
            self.ser = serial.Serial(
                    # Serial Port to read the data from
                    port=self.port,
                    #Rate at which the information is shared to the communication channel
                    baudrate = self.monitor_speed)
            
            
            self.monitor_speed = int(self.e_monitorspeed.get())
            
            port_toplevel.destroy()
            
            
            message = "SETUP "+str(self.monitor_speed)+"\n"
            self.ser.write(message.encode())
            time.sleep(0.2)
            self.ser.write(message.encode())
            
            self.ser = serial.Serial(
                    # Serial Port to read the data from
                    port=self.port,
                    #Rate at which the information is shared to the communication channel
                    baudrate = self.monitor_speed)
            
            self.ser.timeout = 10
            
            self.start_preview()
            
        submitB = ctk.CTkButton(port_toplevel, command = submit_port, text="Potvrdit")
        submitB.grid(row=2, column=0, pady=10, columnspan=2 , sticky="nesw")
        

        self.statusL = ctk.CTkLabel(self.EKG_window, text="Start programu")
        self.statusL.grid(row=0, column = 3)
        
        nadpis = ctk.CTkLabel(master=self.EKG_window, justify=ctk.LEFT, text="Měření z domácího EKG", font=("Futura", 40))
        nadpis.grid(row=0, column=7, columnspan=6, rowspan = 2, pady=(25,0), padx = (20,20))

        self.app.update()
        

    def start_preview(self):
        self.previewbool = not self.previewbool
        print(self.previewbool)
        if(self.previewbool == True):
            self.x = threading.Thread(target=self.preview)
            self.x.start()
        else:
            zprava = ""
            self.statusL.configure(text="Vypínání preview")
            self.ser.write("Preview OFF\n".encode())
            while str(zprava) != "Preview STOP": 
                
                x=self.ser.readline()
                zprava = x.decode('utf-8').strip()
                print(zprava)
            self.statusL.configure(text="Preview vypnuto")
    
    
    def start_measure(self):
        self.measurebool = True
        self.EKGcounter=0
        self.merenibool = True
        self.previewbool = False
        self.EKGminicheck = True
        self.thread2 = threading.Thread(target=self.measure)
        
        self.statusL.configure(text="Vypínání preview")
        zprava = ""
        
        while str(zprava) != "Preview STOP": 
            self.ser.write("Preview OFF\n".encode())
            x=self.ser.readline()
            zprava = x.decode('utf-8').strip()
            print(zprava)
        self.statusL.configure(text="Preview vypnuto")
        
        self.thread2.start()
    
    def EKG_statusbar_update(self):
        if self.measurebool == True:
            if self.countdownbool == True:
                self.EKGcounter = self.EKGcounter + 1
                self.status_configure(text="Příprava na měření "+str(10-self.EKGcounter)+" s")
                print(self.EKGcounter)
            elif(self.merenibool == True):
                self.EKGcounter = self.EKGcounter + 1
                self.statusbar_set(value=self.EKGcounter/300)
                self.status_configure(text="Probíhá měření "+str(self.EKGcounter)+"/300 s")
                #self.app.after(1000, self.EKG_statusbar_update)
            else:
                self.EKGcounter = self.EKGcounter + 1
                self.status_configure(text="Probíhá zapisování "+str(self.EKGcounter)+" s")
                print(self.EKGcounter)
            self.app.after(1000, self.EKG_statusbar_update)
        
    def measure(self):
        if(self.measurebool == True):
            self.countdownbool = True
            self.EKG_window.destroy()
            self.app.after(1000, self.EKG_statusbar_update)
            time.sleep(10)
            
            
            
            
            zprava = ""
            #self.statusL.configure(text="Zapínání měření")
            self.ser.write("Measure ON\n".encode())
            start_time = time.time()
            
            while (str("".join(re.split("[^a-zA-Z]*", str(zprava)))) != "MeasureSTART"): #zprava != "Measure START"
                
                x=self.ser.readline()
                zprava = x.decode('utf-8').strip()
                print("".join(re.split("[^a-zA-Z]*", str(zprava))))
                
                if time.time() - start_time > 10:
                    print("Timeout reached! The while loop took too long.")
                    CTkMessagebox(title="Error", message="Chyba! Program nenastartoval! Restartujte program a ESP", icon="cancel")
                    self.status_configure(text="Měření selhalo...")
                    self.measurebool=False
                    break
            
            self.countdownbool = False
            self.EKGcounter=0
            #self.statusL.configure(text="Měření zapnuto")
            
            print("Měření EKG signálu...")
            
            
            #self.status_configure(text="Měření EKG signálu...")
            
            
            data = []
            startbool = False
            
            
            while self.measurebool == True:
                x=self.ser.readline()
                x = x.decode('utf-8').strip()
                
                
                if(startbool == True and str(x) != "START"):
                    if("".join(re.split("[^a-zA-Z]*", str(x))) == "KONEC"):
                        print("UAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
                        print("".join(re.split("[^0-9]*", str(x))))
                        self.EKGdatarate = str("".join(re.split("[^0-9]*", str(x))))
                        self.EKG_delkamereni.configure(text="Délka měření: "+str(self.EKGdatarate) + " ms")
                        self.status_configure(text="Zápis do souboru...")

                        with open(self.EKGmini_filename.get()+'.csv', 'w') as outfile:
                            outfile.write('\n'.join(str(i) for i in data))
                        print("File written " + self.EKGmini_filename.get()+'.csv')
                        data = []
                        
                        self.measurebool = False
                        self.status_configure(text="Hotovo :)")

                        
                        f = open(self.EKGmini_filename.get()+'.csv', mode="rb")
                        data = f.read()
                        
                        # Dokóduj byte string na klasický řetězec
                        text_string = data.decode('utf-8')
                        
                        # Najdi všechna čísla v souboru a ulož je do pole
                        hodnoty_y = [int(match.group()) for match in re.finditer(r'\d+', text_string)]

                        self.EKG_pocetradku.configure(text="Počet řádků: " + str(len(hodnoty_y)))
                        f.close()
                    
                    else:
                        if(self.measurebool == True):
                            try:
                                data.append(int(x))
                                
                            except:
                                print("Error", x)
                
                if(str(x) == "START"):
                    startbool = True
                    self.merenibool = False
                    self.EKGcounter = 0
                    self.status_configure(text="Čtení z UART...")
                    print("start")
                    
                    
                

    def preview(self):
        if(self.previewbool == True):
            startbool = False
            zprava = ""
            self.statusL.configure(text="Zapínaní preview")
            

            start_time = time.time()
            self.ser.timeout = 10
            while str(zprava) != "Preview START": 
                self.ser.write("Preview ON\n".encode())
                
                x=self.ser.readline()
                print(x)
                try:
                    zprava = x.decode('utf-8').strip()
                except:
                    print("Nejde dekodovat do UTF-8")
                print("".join(re.split("[^a-zA-Z]*", str(zprava))))
                
                if time.time() - start_time > 10:
                    print("Timeout reached! The while loop took too long.")
                    CTkMessagebox(title="Error", message="Chyba! Program nenastartoval! Restartujte program a ESP", icon="cancel")
                    break

                
            self.statusL.configure(text="Preview zapnuto")
            data = []
            counter = 0
            transfer_time = 0
            
            while self.previewbool == True:
                x=self.ser.readline()

                x = x.decode('utf-8').strip()
            
                
                if(startbool == True and "".join(re.split("[^a-zA-Z]*", str(x))) != "START"):
                    if("".join(re.split("[^a-zA-Z]*", str(x))) == "KONEC"):
                        print("konec: "+ "".join(re.split("[^0-9]*", str(x))))
                        
                        
                        print("Pocet Radku: "+str(counter))
                        
                        self.previewstatus_l.configure(text=str(counter)+"; "+str("".join(re.split("[^0-9]*", str(x)))+"; "+str(transfer_time)))
                        counter=0
                        
                        self.ax.cla()
                        
                        data = [((i*950)/3550) for i in data]
                        
                        self.ax.plot(data)
                        self.ax.set_xlabel("Čas [ms]")
                        self.ax.set_ylabel("Napětí [mV]")
                        self.canvas.draw()
                        data = []
                        
                    
                    else:
                        if(self.previewbool == True):
                            try:
                                counter = counter+1
                                
                                data.append(int(x))
                            except:
                                print("Error", x)
                
                if("".join(re.split("[^a-zA-Z]*", str(x))) == "START"):
                    startbool = True
                    
                    print("start" + "".join(re.split("[^0-9]*", str(x))))
                    transfer_time = "".join(re.split("[^0-9]*", str(x)))
                    
                    
    
    

root = Draw_tkinter()
root.init()
    