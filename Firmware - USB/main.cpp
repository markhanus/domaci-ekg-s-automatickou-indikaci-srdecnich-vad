
/*
SOČ 2024 - Domácí EKG s automatickou indikací srdečních vad
  Firmware ESP32 s komunikací po USB

autor: Marek Hanus
licence: MIT License: https://mit-license.org/

*/

#include <Arduino.h>
#include <driver/adc.h>


#define PREVIEW_TIME 2000
#define MEASURE_TIME 300000
#define AVG_NUM 21


int adcRaw = 0;
int baudrate = 0;
int16_t *adcPreview = (int16_t *) ps_malloc(PREVIEW_TIME * sizeof(int16_t));
int *adcData = (int *) ps_malloc(MEASURE_TIME * sizeof(int));
int start, stop, cylce = 0;
int maxtime=0, mintime=10000000;
int start2 = 0, stop2=0;
bool previewmode = false, measuremode=false;
int milisekunda = 0;

void setup(){
  Serial.begin(115200); // 921600
  psramInit();

  adc1_config_width(ADC_WIDTH_BIT_12);
  pinMode(35, INPUT);
}

int extractBaudRate(String command) {
  int index = command.indexOf(" ");
  if (index != -1) {
    return command.substring(index + 1).toInt();
  }
  return 115200; // Return an invalid baud rate if the format is incorrect
}

void read_serial()
{
  if (Serial.available()) { // if there is data comming
    String command = Serial.readStringUntil('\n'); // read string until newline character
    //Serial.println(command);
    if(command == "Measure ON")
    {
      previewmode = false;

      Serial.println("Measure START");
      delay(40);
      Serial.println("Measure START");
      delay(40);
      Serial.println("Measure START");

      measuremode = true;
      
    }
    else if(command == "Measure OFF")
    {
      Serial.println("Measure STOP");
      delay(40);
      Serial.println("Measure STOP");
      
      previewmode = false;
      measuremode = false;
    }
    else if(command == "Preview ON")
    {
      previewmode = true;
      Serial.println("Preview START");
      delay(40);
      Serial.println("Preview START");
    }
    else if(command == "Preview OFF")
    {
      previewmode = false;
      Serial.println("Preview STOP");
      delay(40);
      Serial.println("Preview STOP");
    }

    else if (command.startsWith("SETUP")) {
      // Extract the baud rate from the command
      baudrate = extractBaudRate(command);
      Serial.end();
      Serial.begin(baudrate);
      delay(500);
      Serial.println("Serial set");
      Serial.println("Serial set");
    }

    else
    {
      Serial.print("Unknown command "); Serial.println(command);
    }
  }
}

void run_preview()
{
  adcPreview = (int16_t *) ps_calloc(PREVIEW_TIME, sizeof(int16_t));
  adc1_config_channel_atten(ADC1_CHANNEL_5, ADC_ATTEN_0db);
  start = millis();
  for(int j = 0; j<PREVIEW_TIME; j++)
  {
    milisekunda = millis();
    for(int i = 0; i<21; i++) 
    {
      adcRaw += adc1_get_raw(ADC1_CHANNEL_5);
    }
    while (millis() == milisekunda);
    
    adcPreview[j] = adcRaw/21;
    adcRaw = 0;
  }

  stop = millis();
  Serial.print("START "); Serial.println(stop2-start2);
  start2 = millis();
  for(int j = 0; j<PREVIEW_TIME; j++)
  {
    Serial.println(adcPreview[j]);
    read_serial();
    if(previewmode == false) break;
  }
  stop2 = millis();
  
  //Serial.write((uint8_t *) adcPreview, 2000*sizeof(int16_t));
  free(adcPreview);
  cylce++;

  Serial.print("KONEC "); Serial.println(stop-start);
}

void run_measure()
{
  previewmode = false;
  adcData = (int *) ps_calloc(MEASURE_TIME, sizeof(int));
  adc1_config_channel_atten(ADC1_CHANNEL_5, ADC_ATTEN_0db);
  start = millis();
  for(int j = 0; j<MEASURE_TIME; j++)
  {
    milisekunda = millis();
    for(int i = 0; i<21; i++) 
    {
      adcRaw += adc1_get_raw(ADC1_CHANNEL_5);
    }
    while (millis() == milisekunda);
    
    adcData[j] = adcRaw/21;
    adcRaw = 0;
  }

  stop = millis();
  Serial.println("START");
  
  for(int j = 0; j<MEASURE_TIME; j++)
  {
    Serial.println(adcData[j]);
  }
  free(adcData);
  cylce++;
  Serial.print("KONEC "); Serial.println(stop-start);

  measuremode = false;
}

void loop(){
  if(measuremode == true)
  {
    run_measure();
  }
  else if(previewmode == true)
  {
    run_preview();
  }
  else
  {
    delay(1);
  }

  read_serial();
}